# -*- coding: utf-8 -*-
"""
/*=========================================================================
 *
 *  Copyright elastixteam and contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0.txt
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *=========================================================================*/
 
NFBIA summer school registration workshop

@author: Floris Berendsen
"""
import matplotlib.pyplot as plt


#runsolutions=['intro']
#runsolutions=['1','2','3a','3b','4a','4b','4c','5a','5b']
runsolutions=['5a']
#runsolutions=['1']
#runsolutions=['3b']
oldfigures= plt.get_fignums()
#figsizeinches = [12,9]
#figsizeinches = [6,4.5]
figsizeinches = [8,6]
if 'intro' in runsolutions:
    from solution_workshopintro import *
    fignums = plt.get_fignums()
    for n,f in enumerate(set(fignums).difference(oldfigures)):
        fig=plt.figure(f)
        fig.set_size_inches(*figsizeinches,forward=True)
        fig.savefig('solution_workshopintro_%d.pdf'%(n+1))
        fig.savefig('solution_workshopintro_%d.png'%(n+1))
    oldfigures=fignums
if '1' in runsolutions:
    from solution1 import *
    fignums = plt.get_fignums()
    for n,f in enumerate(set(fignums).difference(oldfigures)):
        fig=plt.figure(f)
        fig.set_size_inches(*figsizeinches,forward=True)
        fig.savefig('solution1_%d.pdf'%(n+1))
        fig.savefig('solution1_%d.png'%(n+1))
    oldfigures=fignums
#ax[0].get_figure().
if '2' in runsolutions:
    from solution2 import *
    fignums = plt.get_fignums()
    for n,f in enumerate(set(fignums).difference(oldfigures)):
        fig=plt.figure(f)
        fig.set_size_inches(*figsizeinches,forward=True)
        fig.savefig('solution2_%d.pdf'%(n+1))
        fig.savefig('solution2_%d.png'%(n+1))
    oldfigures=fignums
if '3a' in runsolutions:
    from solution3a import *
    fignums = plt.get_fignums()
    for n,f in enumerate(set(fignums).difference(oldfigures)):
        fig=plt.figure(f)
        fig.set_size_inches(*figsizeinches,forward=True)
        fig.savefig('solution3a_%d.pdf'%(n+1))
        fig.savefig('solution3a_%d.png'%(n+1))
    oldfigures=fignums
if '3b' in runsolutions:
    from solution3b import *
    fignums = plt.get_fignums()
    for n,f in enumerate(set(fignums).difference(oldfigures)):
        fig=plt.figure(f)
        fig.set_size_inches(*figsizeinches,forward=True)
        fig.savefig('solution3b_%d.pdf'%(n+1))
        fig.savefig('solution3b_%d.png'%(n+1))
    oldfigures=fignums
if '4a' in runsolutions:
    from solution4a import *
    fignums = plt.get_fignums()
    for n,f in enumerate(set(fignums).difference(oldfigures)):
        fig=plt.figure(f)
        fig.set_size_inches(*figsizeinches,forward=True)
        fig.savefig('solution4a_%d.pdf'%(n+1))
        fig.savefig('solution4a_%d.png'%(n+1))
    oldfigures=fignums

if '4b' in runsolutions:
    from solution4b import *
    fignums = plt.get_fignums()
    for n,f in enumerate(set(fignums).difference(oldfigures)):
        fig=plt.figure(f)
        fig.set_size_inches(*figsizeinches,forward=True)
        fig.savefig('solution4b_%d.pdf'%(n+1))
        fig.savefig('solution4b_%d.png'%(n+1))
    oldfigures=fignums

if '4c' in runsolutions:
    from solution4c import *
    fignums = plt.get_fignums()
    for n,f in enumerate(set(fignums).difference(oldfigures)):
        fig=plt.figure(f)
        fig.set_size_inches(*figsizeinches,forward=True)
        fig.savefig('solution4c_%d.pdf'%(n+1))
        fig.savefig('solution4c_%d.png'%(n+1))
    oldfigures=fignums

if '5a' in runsolutions:
    from solution5a import *
    fignums = plt.get_fignums()
    for n,f in enumerate(set(fignums).difference(oldfigures)):
        fig=plt.figure(f)
        fig.set_size_inches(*figsizeinches,forward=True)
        fig.savefig('solution5a_%d.pdf'%(n+1))
        fig.savefig('solution5a_%d.png'%(n+1))
    oldfigures=fignums
if '5b' in runsolutions:
    from solution5b import *
    fignums = plt.get_fignums()
    for n,f in enumerate(set(fignums).difference(oldfigures)):
        fig=plt.figure(f)
        fig.set_size_inches(*figsizeinches,forward=True)
        fig.savefig('solution5b_%d.pdf'%(n+1))
        fig.savefig('solution5b_%d.png'%(n+1))
    oldfigures=fignums
                        