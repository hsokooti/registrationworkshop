# -*- coding: utf-8 -*-
"""
/*=========================================================================
 *
 *  Copyright elastixteam and contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0.txt
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *=========================================================================*/
 
NFBIA summer school registration workshop

@author: Floris Berendsen
"""
import numpy as np
import matplotlib.pyplot as plt
import pylab
import registrationworkshop as rw



    
def TestFiniteDifferencesVSAnalytic(fixedImage,movingImage):
    #start = time.time()
    testrange=np.linspace(-5,5,100)
    parameterscales=np.array([0.1,0.1,0.1,0.1,1,1])
    paramorigin = np.array([1,0,0,1,0,0],dtype='float')
    costfig = pylab.figure()
    for parameterindex in xrange(6):
        currentparam=paramorigin.copy()
        parametersweep=testrange*parameterscales[parameterindex]
        costlandscape=[]
        costderivative=[]
        for sweep in parametersweep:
            currentparam[parameterindex]=paramorigin[parameterindex]+sweep
            print(currentparam)
            costlandscape.append(rw.meanSquaredDifference(fixedImage,movingImage,currentparam))
            costderivative.append(rw.meanSquaredDifferenceGradient5(fixedImage,movingImage,currentparam)[parameterindex]) 
        
        ax = costfig.add_subplot(6,1,parameterindex+1)
        ax.plot(parametersweep,costlandscape,label='cost function')
        ax.plot(parametersweep,parameterscales[parameterindex]*np.array(costderivative),label='analytic derivative')
        ax.plot((parametersweep[1:]+parametersweep[0:-1])/2.0,parameterscales[parameterindex]*np.diff(costlandscape)/np.diff(parametersweep),label='finite difference')
    pylab.legend()

    #end = time.time()
    #print 'finiteDifferences'
    #print end - start
    
def TestUpsampleImageWithGradients(fixedImage,factor=4.0):
    upsampledImageWithGrad=dict()
    upsampledImageWithGrad['spacing']= fixedImage['spacing'] / factor
    upsampledImageWithGrad['extent']= fixedImage['extent'] * factor 
    upsampledImageWithGrad['origin'] = fixedImage['origin'] + (upsampledImageWithGrad['spacing']-fixedImage['spacing'])/2.0 
    
    samplinggrid=rw.imageGridToPoints(upsampledImageWithGrad)
    interpolatorValues, isValidPoint = rw.imageInterpolatorWithGradients(fixedImage,samplinggrid)
    
    upsampledImageWithGrad['data']=np.empty(np.append(interpolatorValues.shape[0],upsampledImageWithGrad['extent']),dtype=float)
    upsampledImageWithGrad['data'].fill(np.nan)
    upsampledImageWithGrad['data'][:,isValidPoint]=interpolatorValues
    
    #show_physical_image2D(upsampledImageWithGrad)
    return upsampledImageWithGrad
def TestUpsampleImageWithGradientsRotated(fixedImage,factor=4.0,angle=0.3):
    upsampledImageWithGrad=dict()
    upsampledImageWithGrad['spacing']= fixedImage['spacing'] / factor
    upsampledImageWithGrad['extent']= fixedImage['extent'] * factor 
    upsampledImageWithGrad['origin'] = fixedImage['origin'] + (upsampledImageWithGrad['spacing']-fixedImage['spacing'])/2.0 
    
    samplinggrid=rw.imageGridToPoints(upsampledImageWithGrad)
    
    center = (upsampledImageWithGrad['extent']*upsampledImageWithGrad['spacing'])/2.0
    
    R=rw.rotmat2d(angle)
    parameters=np.append(R.ravel(),R.dot(center)-center)
    
    transformedsamplinggrid=rw.affinetransform2D(samplinggrid,parameters)
    interpolatorValues, isValidPoint = rw.imageInterpolatorWithGradients(fixedImage,transformedsamplinggrid)
    
    upsampledImageWithGrad['data']=np.empty(np.append(interpolatorValues.shape[0],upsampledImageWithGrad['extent']),dtype=float)
    upsampledImageWithGrad['data'].fill(np.nan)
    upsampledImageWithGrad['data'][:,isValidPoint]=interpolatorValues
    return upsampledImageWithGrad

    #show_physical_image2D(upsampledImageWithGrad)
showcases=[
            #'simpleImage', 
            #'upsampleImage', 
            #'upsampleAndRotate', 
            #'FiniteDifferencesSimple',
            #'FiniteDifferencesUpsampled',
            'selfOptimize', 
            'pairOptimize'
            ]


simpleImage= rw.createImage(origin=np.array([0,0]), spacing = np.array([1,1]), extent = np.array([10,10]), data = np.random.rand(10,10))

if 'simpleImage' in showcases:
    print('show simpleImage')
    rw.show_physical_image2D(simpleImage)
    pylab.title('simpleImage')
    
upsampledImageWithGradients = TestUpsampleImageWithGradients(simpleImage,factor=4.0)

if 'upsampleImageGradient' in showcases:
    print('show upsampleImageGradient')
    rw.show_physical_image2D(upsampledImageWithGradients)
    pylab.suptitle('upsampleImageGradient')
    
if 'upsampleAndRotate' in showcases:
    print('show upsampleAndRotate')
    upsampledAndRotated = TestUpsampleImageWithGradientsRotated(simpleImage)
    rw.show_physical_image2D(upsampledAndRotated)
    pylab.suptitle('upsampleAndRotate')

upsampledImage=rw.createImage(**upsampledImageWithGradients)
upsampledImage['data']=upsampledImage['data'][0,...]

if 'upsampledImage' in showcases:
    print('show upsampledImage')
    ax = rw.show_physical_image2D(upsampledImage)
    ax.set_title('upsampledImage')

if 'FiniteDifferencesSimple' in showcases:
    TestFiniteDifferencesVSAnalytic(simpleImage,simpleImage)
    pylab.suptitle('FiniteDifferencesSimple')
if 'FiniteDifferencesUpsampled' in showcases:
    TestFiniteDifferencesVSAnalytic(simpleImage,upsampledImage)
    pylab.suptitle('FiniteDifferencesUpsampled')


identityParameters= np.array([1,0,0,1,0,0],dtype='float')

    
if 'gradients' in showcases:
    testrange=np.linspace(-5,5,100)
    parameterscales=1
    costgradientlandscape=rw.parameterscales*np.array([rw.meanSquaredDifferenceGradient(simpleImage,upsampledImage,np.array([1,0,0,1,0,tx],dtype=float)) for tx in np.linspace(-5,5,100)])
    costgradientfig = pylab.figure()
    costgradientfig.add_subplot(1,1,1).plot(testrange,costgradientlandscape)
if 'selfOptimize' in showcases:
    # We know that the optimum is at the parameters of an Identity transform
    # Test the optimizer by starting at a slightly different transform
    groundTruthParameters=identityParameters
    initialParameters = np.array([1.1,0,0,1.1,0.4,0.4],dtype='float')
    parameterScales = np.array([0.01,0.01,0.01,0.01,1,1],dtype='float')
    numberOfIterations=50
    learningRate = 1.0
    centerOfRotation = rw.getCenterOfImage(simpleImage)
    optimum, perIteration = rw.optimize(simpleImage,simpleImage, initialParameters, learningRate, numberOfIterations, parameterScales, centerOfRotation, calculateValue=True)

    metricConvergenceFig = pylab.figure()
    metricConvergenceAx = metricConvergenceFig.add_subplot(1,1,1)
    metricConvergenceAx.plot(perIteration['metricPerIteration'])
    metricConvergenceAx.set_xlabel('Iteration')
    metricConvergenceAx.set_ylabel('Metric Value')
    
    numberOfParameters = initialParameters.size
    parameterConvergenceFig = pylab.figure()
    parameterConvergenceFig.suptitle('Parameter values per Iteration')
    for pIndex in xrange(numberOfParameters):   
        parameterConvergenceAx = parameterConvergenceFig.add_subplot(numberOfParameters,1,pIndex+1)
        parameterConvergenceAx.plot(perIteration['parametersPerIteration'][:,pIndex], label = 'parameter value')
        parameterConvergenceAx.set_ylabel('p[%d]' % pIndex)
        parameterConvergenceAx.hlines(groundTruthParameters[pIndex],0,numberOfIterations, label = 'ground truth')
    parameterConvergenceAx.set_xlabel('Iteration')
    parameterConvergenceAx.legend()
    print("optimum:", optimum)

print('generating image pair...')
fixedimg, movingimg, groundTruthParameters, groundTruthCenter= rw.synthesizePair(showFig=True)
gtMovingimg = rw.resampleMovingToFixed2D(movingimg,fixedimg,parameters=groundTruthParameters,centerOfRotation=groundTruthCenter)
rw.show_physical_image2D(gtMovingimg)
#groundTruthParameters=identityParameters
#show_physical_image2D(fixedimg)
#show_physical_image2D(movingimg)
    
if 'pairOptimize' in showcases:
    print('register pair')
    initialParameters = identityParameters
    parameterScales = np.array([0.01,0.01,0.01,0.01,1,1],dtype='float')
    learningRate = 5.0   
    numberOfIterations=50
    centerOfRotation=rw.getCenterOfImage(fixedimg)
    optimum, perIteration = rw.optimize(fixedimg,movingimg, initialParameters, learningRate, numberOfIterations, parameterScales,centerOfRotation,calculateValue=True)   
    print("optimum at:", optimum)   
    resampledI = rw.resampleMovingToFixed2D(movingimg,fixedimg,optimum)
    ax = rw.show_physical_image2D(fixedimg,alpha=1)
    ax = rw.show_physical_image2D(resampledI,alpha=1)

    groundTruthWithNewCenter = rw.parametersNewCenterOfRotation(groundTruthParameters,groundTruthCenter,centerOfRotation)
    metricConvergenceFig = pylab.figure()
    metricConvergenceAx = metricConvergenceFig.add_subplot(1,1,1)
    metricConvergenceAx.plot(perIteration['metricPerIteration'])
    metricConvergenceAx.set_xlabel('Iteration')
    metricConvergenceAx.set_ylabel('Metric Value')
    
    numberOfParameters = initialParameters.size
    parameterConvergenceFig = pylab.figure()
    parameterConvergenceFig.suptitle('Parameter values per Iteration')
    for pIndex in xrange(numberOfParameters):   
        parameterConvergenceAx = parameterConvergenceFig.add_subplot(numberOfParameters,1,pIndex+1)
        parameterConvergenceAx.plot(perIteration['parametersPerIteration'][:,pIndex], label = 'parameter value')
        parameterConvergenceAx.set_ylabel('p[%d]' % pIndex)
        parameterConvergenceAx.hlines(groundTruthWithNewCenter[pIndex],0,numberOfIterations, label = 'ground truth')
    parameterConvergenceAx.set_xlabel('Iteration')
    parameterConvergenceAx.legend()
    