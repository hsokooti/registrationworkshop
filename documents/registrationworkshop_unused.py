# -*- coding: utf-8 -*-
"""
/*=========================================================================
 *
 *  Copyright elastixteam and contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0.txt
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *=========================================================================*/
 
NFBIA summer school registration workshop

@author: Floris Berendsen
"""
import pylab
import numpy as np
from scipy.ndimage import gaussian_filter
import SimpleITK as sitk
import time

#m = np.array(np.meshgrid(t,t,indexing='ij'))
def readImage(filename):
    """ 
    Read an image in our workshop format. It can read intensity (scalar) and 
    deformation field (vector) images. The reader uses ITK and therefore 
    supports various file formats: http://www.itk.org/Wiki/ITK/File_Formats
    
    Parameters
    ----------
    filename : str
        name of the file to be read
        
    Returns
    -------
    image : dict
        image in our workshop format
    """
    itkImg = sitk.ReadImage(filename)
    origin = itkImg.GetOrigin()[::-1]
    spacing = itkImg.GetSpacing()[::-1]
    extent = itkImg.GetSize()[::-1]    
    data = sitk.GetArrayFromImage(itkImg)

    #if itkImg.GetNumberOfComponentsPerPixel() > 1:
        # if Vector image
        # Convert data to store the vector components at the zeroth axis
        # The data is not copied, but a different view is generated
    #    data = np.rollaxis(data,itkImg.GetDimension()) 
        
    return createImage(origin,spacing,extent,data)    

def createImage(origin, spacing, extent, data):
    """
    Returns an image defined with physical position and dimensions in our
    workshop format
    
    Returns a dictionary that bundles Image intensities with the spatial
    properties origin, spacing and extent. For convenience this function checks
    if the dimensionality of all the argmuments are consistent.

    Parameters
    ----------
    origin : array_like
        Position of the first voxel in the world coordinates. A voxel position
        is refered to as its center.
    spacing : array_like
        size of the voxels in physical units, e.g. mm
    extent : array_like
        the number of voxels in each direction
    data : array_like 
        The data at the voxels. For an intensity image data.shape must be equal
        to extent. For a vector image data.shape = ( vdims, xdims, ydims, ... ) 
        whereas extent = [ xdims, ydims, ... ], with vdims the dimensionality of 
        the vector.
             
    Returns
    -------
    image : dictionary
        All properties are stored as nd-arrays. The indiviual elements of the 
        arrays cannot be written to. To change properties a new array must be 
        assigned.
        
    Notes
    -----
    We use a dictionary instead of a dedicated class for its ease of use with 
    Spyder' variable explorer.
    An Image dictionary can be manually created by:
    
    >>> image = {'origin' : origin, 'spacing' : spacing, 'extent' : extent, 
                 'data' : data}
    
    A (shallow) copy of an image can be made:
    
    >>> secondImage = createImage(**firstImage)

    Examples
    --------
    
    >>> createImage(origin=np.array([0.0,0.0]),
                    spacing=np.array([1.0,1.0]),
                    extent=np.array([1.0,1.0]),
                    data=np.array([[0.0]]))
    >>> createImage([0.0,0.0],[1.0,1.0],[1.0,1.0],[0.0])
    >>> createImage([0.0,0.0],[1.0,1.0],data = np.array[[0.0]], 
                    extent=(1,1))
    
    """
    
    # Convert lists, tuples and integer arrays to float arrays
    # Try to keep the original references to the data, i.e. without copying
    origin=np.asarray(origin,dtype=float)    
    spacing=np.asarray(spacing,dtype=float)
    extent=np.asarray(extent,dtype=int)
    data=np.asarray(data,dtype=float)
    # After creating an image the individual elements of the array defining the
    # world coordinates cannot be changed anymore. For changes you need to 
    # reassign a new array 
    origin.setflags(write=False)    
    spacing.setflags(write=False)    
    extent.setflags(write=False)    
    data.setflags(write=False)    

    if (not origin.shape == spacing.shape) or (not origin.shape == extent.shape):
        raise ValueError("The shapes of origin, spacing and extent must be equal. Found %s %s %s" % (repr(origin.shape), repr(spacing.shape), repr(extent.shape)))
    if (len(data.shape)>(extent.size+1)) or (not data.shape[:extent.size] == tuple(extent)):
        raise ValueError("The dimensionality of the data is incompatible with the extent. Found %s %s" % (repr(data.shape), tuple(extent)))
    return {'origin' : origin, 'spacing' : spacing, 'extent' : extent, 'data' : data}
    

def getNeighborhood(imageData,index):    
    """ 
    Returns the n-dimensional patch of 2x2x... image data with its lowercorner at index
    Patches lying (partly) outside the image extent are filled with nearest pixels (zero flux Neumann boundary condition)
    Function is to be used with the linear interpolator. 
    """
    ndneighborhoodindex = [np.array([c,c+1],dtype=int,ndmin=direction+1).T for direction,c in enumerate(index[::-1])][::-1]
    ndneighborhoodindex = [np.clip(indices,0,imageData.shape[direction]-1) for direction, indices in enumerate(ndneighborhoodindex)]
    return imageData[ndneighborhoodindex]

def getNeighborhoodView(imageData,index):
    # Similar to getNeighborhood(), but returns a 'view' to the data, i.e. no 
    # copy is made and writing to the neigborhood array changes the underlying 
    # image data.
    # No boundary conditions apply and an Error is thrown if the neighborhood 
    # reaches over the boundary
    ndneighborhoodindex=[np.s_[lowerIndex:lowerIndex+2] for lowerIndex in index]
    return imageData[ndneighborhoodindex]
    
def intensityAt(continuousIndex,imageData):
    """ 
    Linear Interpolation of the image data at a (real valued) continuousIndex in image data space 
    Parameters
    ----------
    continuousIndex : array_like
        position in (real valued) voxel coordinates. 
    imageData : array_like
        intensity values of the image, e.g. image['data']
    extent : array_like

    Returns
    -------
    intensity : scalar_like
        interpolated value
    """
    index = np.floor(continuousIndex)
    remainderIndex = continuousIndex-index
    neighborhood = getNeighborhood(imageData,index)
    for rem in remainderIndex[::-1]:
        interpIntensityKernel=np.array([1.-rem,rem])
        neighborhood = neighborhood.dot(interpIntensityKernel)
    return neighborhood

def derivativeAt(continuousIndex,imageData,direction):
    """ 
    Calculate the derivative value of the image data at a continuousIndex given 
    a linear interpolating kernel. 
    Parameters
    ----------
    continuousIndex : array_like
        position in (real valued) voxel coordinates. 
    imageData : array_like
        intensity values of the image, e.g. image['data']
    extent : array_like

    Returns
    -------
    imageDataDerivative : scalar_like
        derivative of the image along the "direction" axis. The magnitudes of 
        the derivatives are relative to the imageData space
    """
    index = np.floor(continuousIndex)
    remainderIndex = continuousIndex-index
    neighborhood = getNeighborhood(imageData,index)
    directionalKernels = [np.array([-1.,1.]) if d==direction else np.array([1.-rem,rem]) for d,rem in enumerate(remainderIndex)]
    for kernel in directionalKernels[::-1]:
        neighborhood = neighborhood.dot(kernel)       
    return neighborhood
 

def intensityAndGradientAt(continuousIndex,imageData):
    """ 
    Interpolate the image data at a (real valued) continuousIndex in image data space 
    Parameters
    ----------
    continuousIndex : array_like
        position in (real valued) voxel coordinates. 
    imageData : array_like
        intensity values of the image, e.g. image['data']

    Returns
    -------
    imageDataDerivative : scalar_like
        derivative of the image along the "direction" axis. The magnitudes of 
        the derivatives are relative to the imageData space
    """

    index = np.floor(continuousIndex)
    remainderIndex = continuousIndex-index
    #dimensions = continuousIndex['extent']
    value = getNeighborhood(imageData,index)
    #neighborhoodPerDirection=[neighborhood for dummy in xrange(dimensions)]
    derivatives=[]
    #for derivativeDirection in xrange(dimensions):
    
    
    # v  dx dy dz : value, derivativeX, derivativeY, derivativeZ
    # -- -- -- -- 
    # iz       dz : using value neighborhood of previous iteration, derivatieve in z is calculated and value is interpolated in z
    # iy    dy iy : using value neighborhood of previous iteration, derivatieve in y is calculated and value and dz are interpolated in y
    # iz dx ix ix : using value neighborhood of previous iteration, derivatieve in x is calculated and value, dy and dz are interpolated in x
    
    # loop over all dimension directions 
    # get the fractional part of each coordinate
    for rem in remainderIndex[::-1]: 
        # the dot operator 'eats' dimensions by starting with the last dimension
        #calculate the interpolating kernel for this dimension direcion
        # a linear interpolating kernel is \/
        interpIntensityKernel=np.array([1-rem,rem])  
        #the derivatives of all previous directions are interpolated by the current direction
        derivatives=[derivative.dot(interpIntensityKernel) for derivative in derivatives]
        #the current directional derivative is obtained by the previous interpolated value 
        # a linear derivative kernel is __--
        derivatives.append(value.dot(np.array([-1,1])))
        value = value.dot(interpIntensityKernel)    
    #return value, np.array(derivatives)[::-1]
    return np.array([value]+derivatives[::-1])
    
def gradientAt(continuousIndex,imageData):
    """ 
    Calculate the derivative value of the image at a continuousIndex given 
    a linear interpolating kernel. 
    Parameters
    ----------
    continuousIndex : array_like
        position in (real valued) voxel coordinates. 
    imageData : array_like
        intensity values of the image, e.g. image['data']

    Returns
    -------
    imageDataDerivative : scalar_like
        derivative of the image along the "direction" axis. The magnitudes of 
        the derivatives are relative to the imageData space
    """
    index = np.floor(continuousIndex)
    remainderIndex = continuousIndex-index
    #dimensions = continuousIndex['extent']
    value = getNeighborhood(imageData,index)
    #neighborhoodPerDirection=[neighborhood for dummy in xrange(dimensions)]
    derivatives=[]
    #for derivativeDirection in xrange(dimensions):
    
    # loop over all dimension directions 
    # get the fractional part of each coordinate
    for rem in remainderIndex[:0:-1]: 
        #calculate the interpolating kernel for this dimension direcion
        # a linear interpolating kernel is \/
        interpIntensityKernel=np.array([1-rem,rem])  
        #the derivatives of all previous directions are interpolated by the current direction
        derivatives=[derivative.dot(interpIntensityKernel) for derivative in derivatives]
        #the current directional derivative is obtained by the previous interpolated value 
        # a linear derivative kernel is __--
        derivatives.append(value.dot(np.array([-1,1])))
        value = value.dot(interpIntensityKernel)    

    interpIntensityKernel=np.array([1-remainderIndex[0],remainderIndex[0]])  
    #the derivatives of all previous directions are interpolated by the current direction
    derivatives=[derivative.dot(interpIntensityKernel) for derivative in derivatives]
    #the current directional derivative is obtained by the previous interpolated value 
    # a linear derivative kernel is __--
    derivatives.append(value.dot(np.array([-1,1])))
        
    #return value, np.array(derivatives)[::-1]
    return np.array(derivatives[::-1])

def resampleIntensities(continuousIndices,imageData):
    """
    Interpolate imageData for each continuousIndex in continuousIndices.
    
    Parameters
    ----------
    continuousIndices : array_like
        positionss in (real valued) voxel coordinates. The required shape is 
        (pointDimension,...), i.e. the coordinates of indices are stored on 
        the zeroth axis, while the other axis serve as a multidimensional list 
        that is preserved in the output
    imageData : array_like
        intensity values of the image, e.g. image['data']

    Returns
    -------
    imageDataDerivative : array_like
        derivative of the image along the "direction" axis. The magnitudes of 
        the derivatives are relative to the imageData space
    """
    return np.apply_along_axis(intensityAt,-1,continuousIndices,imageData)

def resampleDerivative(continuousIndices,imageData,direction):
    return np.apply_along_axis(derivativeAt,-1,continuousIndices,imageData,direction)

def resampleGradients(continuousIndices,imageData):
    return np.apply_along_axis(gradientAt,-1,continuousIndices,imageData)

def resampleIntensitiesAndGradients(continuousIndices,imageData):
    return np.apply_along_axis(intensityAndGradientAt,-1,continuousIndices,imageData)

#def putToZerothAxis(arr,dims):
    #np.array(imageData['origin'],ndmin=imageData['data'].ndim+1).T
    #arr.shape=tuple([1]+[0]*(dims-1))
 #   return np.asarray(arr).reshape(tuple([-1]+[1]*(dims-1)))

def pointsToContinuousIndices(points,image):
    # point accepts arrays where [x,y,z] are accesed by the 0th axis, e.g. a meshgrid
    points = np.atleast_2d(points)
    continuousIndices = (points-image['origin'])/image['spacing']
    isValidPoint=np.prod((continuousIndices>=-0.5) * (continuousIndices < 
        image['extent']-0.5), axis=-1,dtype=bool)
    #return imagecoords[:,isValidPoint], isValidPoint    
    return continuousIndices[isValidPoint,:], isValidPoint    
    #imagecoords[(imagecoords<0.5) + (imagecoords > (putToZerothAxis(image['data'].shape,point.ndim)+0.5)) ]=np.nan

def imageGridToPoints(image,**kwargs):
    if 'spacing' in kwargs:
        spacing = kwargs['spacing']
    else:
        spacing = image['spacing']
    if 'origin' in kwargs:
        origin = kwargs['origin']
    else:
        origin = image['origin']
    if 'shape' in kwargs:
        shape = kwargs['shape']
    else:
        shape= image['extent'] #np.array(image['data'].shape)
    axesgrid=[np.linspace(origin_elem,origin_elem+(shape_elem-1)*spacing_elem,shape_elem) for origin_elem, shape_elem, spacing_elem in zip(origin,shape,spacing)]
    #print axesgrid    
    return np.array(np.meshgrid(*axesgrid)).T
def imageInterpolator(image, points, fillvalue=np.nan):
    """
    Interpolates image at each point in points.
    
    Returns
    -------
    imageData : array_like
        
    
    
    Parameters
    ----------
    image : dict (in workshop format)
        contains the data at each voxel and the position in physical space
    points : array-like
    
    fillvalue : scalar (optional)
        intensity values outside the image domain.
        default = NaN
    
    See also
    --------
    imageInterpolatorSparse
    
    Examples
    --------
    >>> interpvals, isValidpoint = rw.imageInterpolator(img, gridpoints)
    To create an image:
    >>> resultImg = np.zeros(isValidPoint.shape)
    >>> resultImg[isValidPoint] = interpvals
    """
    #points=np.atleast_2d(points)
    continuousIndices, isValidPoint = pointsToContinuousIndices(points,image)
    intensities = np.apply_along_axis(intensityAt,-1,continuousIndices,image['data'])
    resultImageData = np.empty(isValidPoint.shape,dtype=float)
    resultImageData.fill(fillvalue)
    resultImageData[isValidPoint] = intensities
    
    return resultImageData

def imageInterpolatorSparse(image,points):
    """
    Interpolates image at each point in points.
    
    Returns
    -------
    sparseImageData : array-like
        sampled intensities only for points that lie inside the image domain.
    isValidPoint : bool array 
        
    
    Parameters
    ----------
    image : dict (in workshop format)
        contains the data at each voxel and the position in physical space
    points : array-like

    See also
    --------
    imageInterpolator
        
    Examples
    --------
    
    >>> interpvals, isValidpoint = rw.imageInterpolator(img, gridpoints)
    To create an image:
    >>> resultImageData = np.zeros(isValidPoint.shape)
    >>> resultImageData[isValidPoint] = interpvals
    """
    continuousIndices, isValidPoint = pointsToContinuousIndices(points,image)
    intensities = np.apply_along_axis(intensityAt,-1,continuousIndices,image['data'])
    return intensities, isValidPoint
    
    
def imageInterpolatorWithGradients(image, points, sparse=True, fillvalue = np.nan ):
    continuousIndices, isValidPoint = pointsToContinuousIndices(points,image)
    intensityAndGradients = np.apply_along_axis(intensityAndGradientAt,-1,continuousIndices,image['data'])
    intensityAndGradients[:,1:]/=image['spacing']#[:,None]
    #intensities = np.apply_along_axis(valueAndDerivative2At,0,continuousIndices,image['data'])
    #print(continuousIndices.shape)
    #print(image['data'].shape)
    #intensities = interpolateValAndDer2(continuousIndices,image['data'])
    if sparse == True:
        return intensityAndGradients, isValidPoint
    elif sparse == False:    
        resultImageData = np.empty(isValidPoint.shape + (intensityAndGradients.shape[1],),dtype=float)
        resultImageData.fill(fillvalue)
        resultImageData[isValidPoint,:] = intensityAndGradients
        return resultImageData
def imageInterpolatorGradients(image,points):
    continuousIndices, isValidPoint = pointsToContinuousIndices(points,image)
    intensityGradients = np.apply_along_axis(gradientAt,-1,continuousIndices,image['data'])
    intensityGradients/=image['spacing']#[:,None]
    #intensities = np.apply_along_axis(valueAndDerivative2At,0,continuousIndices,image['data'])
    #print(continuousIndices.shape)
    #print(image['data'].shape)
    #intensities = interpolateValAndDer2(continuousIndices,image['data'])
    return intensityGradients, isValidPoint


def show_physical_image2D(img,alpha=None,ax=None):
    plotExtent=(img['origin'][1]-0.5*img['spacing'][1],
            img['origin'][1]+(img['extent'][1]-0.5)*img['spacing'][1],           
            img['origin'][0]+(img['extent'][0]-0.5)*img['spacing'][0],
            img['origin'][0]-0.5*img['spacing'][0])
    #plotExtent=(img['origin'][1],
    #        img['origin'][1]+(img['extent'][1]-0.5)*img['spacing'][1],
    #        img['origin'][0],
    #        img['origin'][0]+(img['extent'][0]-0.5)*img['spacing'][0])

    plotsettings={'extent':plotExtent,'origin':'upper', 'interpolation': 'nearest','alpha': alpha }
    #,'xlabel' : 'world coordinate []', 'ylabel' : 'world coordinate []'}
    if type(ax)==list:
        for vectorindex, currentax in enumerate(ax):
            currentax.imshow(img['data'][...,vectorindex],**plotsettings)
            
    elif ax == None:
        fig=pylab.figure()        
        ax=[]
        img3d= np.atleast_3d(img['data'])        
        vectorlen=img3d.shape[-1]  
        for vectorindex in xrange(vectorlen):
            currentax=fig.add_subplot(1,vectorlen,vectorindex+1)
            currentax.imshow(img3d[...,vectorindex],**plotsettings)
            ax.append(currentax)
            #fig.colorbar(ax)
    else:
        ax.imshow(img['data'],**plotsettings)
        
    #fig.colorbar(pylab.gca())
    return ax

def translationTransform2D(points,offset):
    return points+offset
    
def rotmat2d(theta):
    return np.array([[np.cos(theta),np.sin(theta)],[-np.sin(theta),np.cos(theta)]])
def affinetransform2D(points,parameters,centerOfRotation):#centerOfRotation
    mat=parameters[:4].reshape([2,2])
    vec=parameters[4:]
    #return (points-rw.putToZerothAxis(centerOfRotation,points.ndim)).T.dot(mat).T + rw.putToZerothAxis(vec,points.ndim) + rw.putToZerothAxis(centerOfRotation,points.ndim)
    return (points-centerOfRotation).dot(mat) + vec + centerOfRotation
    
def affineJacobian2DAt(point,centerOfRotation):#,centerOfRotation):
    #return np.array([[point[0],0],
    #                 [0,point[0]],
    #                 [point[1],0],
    #                 [0,point[1]],
    #                 [  1,   0  ],
    #                 [  0,   1  ]])
    p=point-centerOfRotation
    return np.array([[p[0], 0   , p[1], 0   , 1 , 0],
                     [0   , p[0], 0   , p[1], 0 , 1]],dtype=float)  

def affineJacobianAt(point,centerOfRotation):
    point-=centerOfRotation
    # point.shape = (index,dim)
    # point.shape[-1]: spatial dimensions
    #for 2d it returns: 
    # np.array([[p0, 0  , p1, 0 , 1 , 0],
    #           [0 , p0 , 0 , p1, 0 , 1]])
    return np.hstack([np.eye(point.ndim)*p for p in point.tolist()+[1.0]])   

def synthesizeAffine2D(parameterScales):
    return np.array([1,0,0,1,0,0],dtype=float) + parameterScales*np.random.uniform(-0.5,0.5,6)

def affineToMat(parameters):
    dim = np.sqrt(1.0+4.0*len(parameters))/2.0-0.5
    mat = np.eye(dim+1)
    mat[:dim,:dim]=parameters[:dim*dim].reshape([dim,dim])
    mat[:dim,dim]=parameters[-dim:]
    return np.asmatrix(mat)
def matToAffine(mat):
    dim=mat.shape[0]-1
    parameters=np.empty(dim*(dim+1),dtype=float)
    parameters[:dim*dim] = mat[:dim,:dim].ravel()
    parameters[-dim:] = mat[:dim,dim].T
    return parameters

def parametersNewCenterOfRotation(parameters, oldcenterOfRotation, newCenterOfRotation):
    #pointnew = (pointold-oldcenter) * affineRotMatrix + oldtranslation + oldcenter
    #         = (pointold-newcenter) * affineRotMatrix + newtranslation + newcenter
    # pointold = 0
    # newtranslation = (-oldcenter) * affineRotMatrix + oldtranslation + oldcenter - (-newcenter) * affineRotMatrix  - newcenter 
    
    mat = affineToMat(parameters)
    dim=mat.shape[0]-1
    oldTranslation = mat[:-1,-1].ravel()
    affineRotMatrix = mat[:-1,:-1]
    newTranslation = (-np.array(oldcenterOfRotation)).dot(affineRotMatrix) + oldTranslation + oldcenterOfRotation + np.array(newCenterOfRotation).dot(affineRotMatrix) - np.array(newCenterOfRotation)
    newParameters = parameters.copy()
    newParameters[-dim:]=newTranslation
    return newParameters
  
                  
def meanSquaredDifference(fixedI, movingI, parameters,centerOfRotation):
    samplingGridAtFixed = imageGridToPoints(fixedI)
    transformedSamplingGrid=affinetransform2D(samplingGridAtFixed,parameters,centerOfRotation)
    interpolatorValues, isValidPoint = imageInterpolator(movingI,transformedSamplingGrid)
    return np.mean((fixedI['data'][isValidPoint]-interpolatorValues)**2)

#def chainrule():
#    affineJacobian2D(point,parameters).dot
def meanSquaredDifferenceGradient(fixedI, movingI, parameters, centerOfRotation):
    #dS/dmu = dS/dIm * dIm/dT * dT/dmu
    #dS[dmu] = sum_x( dS/dI|I=M(T(mu;x))  * dI/dT|T=T(mu;x) dot dT/dmu|T=T(x))
    # ([vector 6]) =  sum_x(        [scalar]   * [vector 2 ] dot [matrix 2x6])
    samplingGridAtFixed = imageGridToPoints(fixedI)
    transformedSamplingGrid = affinetransform2D(samplingGridAtFixed,parameters)
    intensityAndGradients, isValidPoint = imageInterpolatorWithGradients(movingI,transformedSamplingGrid)
    parameterGradients=np.zeros_like(parameters)
    
    numberOfValidpoints = intensityAndGradients.shape[0]
    #numberOfValidpoints= np.count_nonzero(isValidPoint)

    for validPointIndex in xrange(numberOfValidpoints):
        fixedInt= fixedI['data'][isValidPoint][validPointIndex]
        movingInt = intensityAndGradients[validPointIndex,0]
        movingGrad = intensityAndGradients[validPointIndex,1:]
        point=transformedSamplingGrid[isValidPoint,:][validPointIndex,:]
        parameterGradients+= -2.0 * (fixedInt - movingInt) * movingGrad.dot( affineJacobian2DAt(point,centerOfRotation) )
        #parameterGradients+= -2.0*(fixedI['data'][isValidPoint][validPointIndex] - intensityAndGradients[0,validPointIndex]) * intensityAndGradients[1:,validPointIndex].dot( affineJacobian2DAt(transformedSamplingGrid[:,isValidPoint][:,validPointIndex]).T )

    # faster:
    #for point, fixedInt, movingIntAndGrad in zip(transformedSamplingGrid[:,isValidPoint].T,fixedI['data'][isValidPoint].T,intensityAndGradients.T):
    #    parameterGradients+= -2.0*(fixedInt - movingIntAndGrad[0]) * movingIntAndGrad[1:].dot( affineJacobian2DAt(point).T )
    parameterGradients/=np.sum(isValidPoint)
    return parameterGradients

    
def meanSquaredDifferenceWithGradient(fixedI, movingI, parameters,centerOfRotation):
    #dS/dmu = dS/dI * dI/dT * dT/dmu
    samplingGridAtFixed = imageGridToPoints(fixedI)
    transformedSamplingGrid = affinetransform2D(samplingGridAtFixed,parameters,centerOfRotation)
    intensityAndGradients, isValidPoint = imageInterpolatorWithGradients(movingI,transformedSamplingGrid)
    parameterGradients=np.zeros_like(parameters)
    
    numberOfValidpoints= intensityAndGradients.shape[0]
    for validPointIndex in xrange(numberOfValidpoints):
        fixedInt= fixedI['data'][isValidPoint][validPointIndex]
        movingInt = intensityAndGradients[validPointIndex,0]
        movingGrad = intensityAndGradients[validPointIndex,1:]
        point=transformedSamplingGrid[isValidPoint,:][validPointIndex,:]
        parameterGradients+= -2.0*(fixedInt - movingInt) * movingGrad.dot( affineJacobian2DAt(point,centerOfRotation) )
 
    # loop over points #nditer?
    #for point, fixedInt, movingIntAndGrad in zip(transformedSamplingGrid[:,isValidPoint].T,fixedI['data'][isValidPoint].T,intensityAndGradients.T):
    #    parameterGradients+= -2.0*(fixedInt - movingIntAndGrad[0]) * movingIntAndGrad[1:].dot( affineJacobian2DAt(point).T )
    parameterGradients/=np.sum(isValidPoint)
    metricValue = np.mean((fixedI['data'][isValidPoint]-intensityAndGradients[...,0])**2)
    return metricValue, parameterGradients
    
def optimize(fixedI, movingI, initialParameters, learningRate, numberOfIterations, parameterScales,centerOfRotation,calculateValue=True):
    parameters = initialParameters
    metricStatistics={}    
    if calculateValue == True:
        metricPerIteration=np.empty(numberOfIterations,dtype=float)
        parametersPerIteration=np.empty((numberOfIterations,parameters.size),dtype=float)
        for iterationNumber in xrange(numberOfIterations):
            metricValue, parameterGradients = meanSquaredDifferenceWithGradient(fixedI, movingI, parameters,centerOfRotation)            
            parameters-=learningRate*parameterScales*parameterGradients
            metricPerIteration[iterationNumber]=metricValue
            parametersPerIteration[iterationNumber,:]=parameters
            print('%d: m=%g, p=%s' % (iterationNumber,metricValue,parameters.__format__('s')))
        metricStatistics={'metricPerIteration': metricPerIteration, 'parametersPerIteration': parametersPerIteration}
    else:
        for iterationNumber in xrange(numberOfIterations):
            parameterGradients = meanSquaredDifferenceGradient(fixedI, movingI, parameters,centerOfRotation)
            parameters-=learningRate*parameterScales*parameterGradients
    return parameters, metricStatistics

    
def blurimage(img,sigma):
    blurred = createImage(data=gaussian_filter(img['data'],sigma), origin = img['origin'], spacing = img['spacing'], extent = img['extent'])
    return blurred
def synthesizeImageData(dims=2,scales=6,state=None):
    if not state == None:
        np.random.set_state(state)
    imageData=np.zeros([1]*dims,dtype=float)
    for resolutionIndex in xrange(scales):
        resolutionScale = 2** resolutionIndex
        nextScale= 2**(resolutionIndex+1)
        ticks = np.linspace(-0.25,resolutionScale-0.75,nextScale)
        continuousIndices = np.array(np.meshgrid(*([ticks]*dims),indexing='ij'))
        imageData = np.apply_along_axis(intensityAt,0,continuousIndices,imageData)
        imageData += np.random.standard_normal([nextScale]*dims)/nextScale
    return imageData

    
def resampleMovingToFixed2D(movingI,fixedI,parameters=None,centerOfRotation=0):
    samplingGridAtFixed = imageGridToPoints(fixedI)
    if parameters == None:
        transformedSamplingGrid = samplingGridAtFixed
    else:
        transformedSamplingGrid = affinetransform2D(samplingGridAtFixed,parameters,centerOfRotation)
    resampledIntensities, isValidPoint = imageInterpolator(movingI,transformedSamplingGrid)
    imageData = np.empty(isValidPoint.shape,dtype=float)
    imageData.fill(np.nan)
    imageData[isValidPoint]=resampledIntensities
    return createImage(fixedI['origin'],fixedI['spacing'],fixedI['extent'],imageData)

def getCenterOfImage(image):
    return image['origin']+image['spacing']*(image['extent']-1)/2.0
    
def synthesizePair(state=None,showFig=False):
    baseImageData = synthesizeImageData(state=state,scales=3)
    baseImage = createImage(np.zeros(2,dtype=float),np.ones(2,dtype=float),baseImageData.shape,baseImageData)
    baseCenter = getCenterOfImage(baseImage)
    
    if showFig==True:
        fig=pylab.figure()
        baseax = fig.add_subplot(1,3,1)
        show_physical_image2D(baseImage,ax=baseax)
        fig.suptitle('Test pair Images')
        gridcolor=['purple','green']

    
    outputImages=[]
    outputTransforms=[]
    
    for outputIndex in xrange(2):
        
        
        # randomly choose the voxel sizes between 0.15 and 0.5 (deliberately upsampling only, to avoid aliasing effect)
        outputSpacing=np.random.uniform(0.15,0.5,2)

        outputMeshTicks = [np.arange(0.15*baseLength-0.5,0.85*baseLength-0.5,spacing) for baseLength, spacing in zip(baseImage['extent'], outputSpacing)]    
        centerROIcontinuousIndices= np.array(np.meshgrid(*outputMeshTicks,indexing='ij'))
    
        parameterScales=np.array([0.2,0.2,0.2,0.2,0.1*baseImage['extent'][0],0.1*baseImage['extent'][1]])
        #print(parameterScales)
        transformParameters = synthesizeAffine2D(parameterScales)
        #print(transformParameters)
        
        #centerROIcontinuousIndices-=putToZerothAxis(baseCenter,len(centerROIcontinuousIndices.shape))
        transformedcontinuousIndices = affinetransform2D(centerROIcontinuousIndices,transformParameters,baseCenter)        
        #transformedcontinuousIndices+=putToZerothAxis(baseCenter,len(transformedcontinuousIndices.shape))
        
        outputOrigin=transformedcontinuousIndices[0,0,:]
        
        resampledIntensities, isValidPoint = imageInterpolator(baseImage,transformedcontinuousIndices)
        outputImageData=np.empty(isValidPoint.shape,dtype=float)
        outputImageData.fill(np.nan)
        outputImageData[isValidPoint]=resampledIntensities
        outputExtent = np.array(outputImageData.shape)
        
        outputImages.append(createImage(outputOrigin,outputSpacing,outputExtent,outputImageData))
        outputTransforms.append(transformParameters)
    
        if showFig==True:
            ax = fig.add_subplot(1,3,2+outputIndex)
            show_physical_image2D(outputImages[-1],ax=ax)        
            #baseax.plot(transformedcontinuousIndices[0,:,:],transformedcontinuousIndices[1,:,:])
            #baseax.plot(transformedcontinuousIndices[0,:,:].T,transformedcontinuousIndices[1,:,:].T)
            baseax.plot(transformedcontinuousIndices[:,:,1],transformedcontinuousIndices[:,:,0],color=gridcolor[outputIndex])
            baseax.plot(transformedcontinuousIndices[:,:,1].T,transformedcontinuousIndices[:,:,0].T,color=gridcolor[outputIndex])
            
    #groundTruthTransform=matToAffine(affineToMat(outputTransforms[0])*(affineToMat(outputTransforms[1]).I))
    groundTruthTransform=matToAffine(affineToMat(outputTransforms[1])*(affineToMat(outputTransforms[0]).I))
    return outputImages[0],outputImages[1], groundTruthTransform, baseCenter
    
    
