%=========================================================================
%
%  Copyright elastixteam and contributors
%
%  Licensed under the Apache License, Version 2.0 (the "License");
%  you may not use this file except in compliance with the License.
%  You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0.txt
%
%  Unless required by applicable law or agreed to in writing, software
%  distributed under the License is distributed on an "AS IS" BASIS,
%  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%  See the License for the specific language governing permissions and
%  limitations under the License.
%
%=========================================================================
% 
% NFBIA summer school registration workshop
%
% author: Floris Berendsen
%
\documentclass[a4paper]{article}
\usepackage[a4paper,inner=10mm,outer=60mm, top=20mm, bottom=40mm,footskip=.5cm,marginparwidth=50mm]{geometry}
\usepackage{amsmath}
\usepackage{graphicx}
%\usepackage{wrapfig}
%\usepackage{capt-of}
\usepackage{marginnote}
\usepackage{sidenotes}
\usepackage{enumitem}
\usepackage[T1]{fontenc} % for the guillemots

\usepackage{color, transparent}
\usepackage[percent]{overpic}

%\usepackage{titlesec}
%\titleformat{\section}
%  {\normalfont}{Assignment \thesection:}{1em}{}
\renewcommand\thesection{Assignment \arabic{section}:}
\renewcommand\thesubsection{\arabic{section}\alph{subsection}:}
	
\newcommand{\argmin}{\arg\!\min}
\author{F.F. Berendsen}
\title{Image Registration Implementation Workshop}
\begin{document}
 \maketitle
 \section*{Introduction}

In this workshop you are going to program a basic 2-D registration algorithm. Programming will be done in the python language which runs in the virtual machine you received.
This document describes the assignments for the workshop. The python assignment scripts can be found in: \texttt{/home/user/imageregistration/implementation/}.
Other sources for this workshop are:
\begin{itemize}
\item Mini reference sheet: python, numpy and this workshop
\item \texttt{registrationworkshop.py}: Library with interpolator and other basic functionality
\item \texttt{mylibrary.py}: collect your functions here
\end{itemize}
Over the course of the assignments we will develop all elements that build up a registration algorithm. The final framework of the algorithm will look like figure \ref{fig:framework}.
\begin{figure}[htb]
\begin{center}
  \includegraphics[width=0.6\textwidth]{framework_gradient.pdf}
  \caption{Overview of the final registration framework} 
  \label{fig:framework}
 \end{center}
\end{figure}

 \section{translate an image (5 min)}
\textbf{Goal}: translate a moving image by an 4 mm in x-direction and -10 mm in y-direction.\\
\textbf{Steps}:
%\begin{enumerate}[label=\alph*)]
\begin{itemize}
\item Implement the function \texttt{translationtransform2D(points,offset)}, such that adds an \guillemotleft offset\guillemotright  to the input points.
\item Create a grid of points, called \texttt{fixedImageGrid}, resembling the fixed image pixel coordinates, using the function \text{rw.imageGridToPoints}.
\item Create a \texttt{transformedGrid} with offset \texttt{[4,-10]} by applying the \texttt{translationtransform2D} to the \texttt{fixedImageGrid}.
\item Use the resample function that you used in the previous exercise to interpolate the moving image at the \texttt{transformedGrid} position.
\item Use the \texttt{show\_physical\_image2D} function to plot the image.
\end{itemize}
%\end{enumerate}
\textbf{Question}: does the image move in the direction that you expected?

 \begin{itemize}
\item When your function \texttt{translationtransform2D(points,offset)} is ready don't forget to copy it to \texttt{mylibrary.py} 
\end{itemize}
\begin{marginfigure}
  \includegraphics[width=\textwidth]{solution1_1.pdf}
  \caption{Solution 1}
 \end{marginfigure}
 
 \section{affine transform (5 min)}
 \textbf{Goal}: transform a moving image by an affine transform.\\
  We define the affine transform as: 
  \[ T_{\mathrm{Affine}}(\mathbf{x};\mathbf{p}) = (\mathbf{x}-\mathbf{c}) \cdot A + \mathbf{c} + \mathbf{t},\]
  with $\mathbf{x}$ a point, $\mathbf{c}$ the center of rotation, $A$ the rotational/scaling/skewing part of the transform and $\mathbf{t}$ the translational part of the transform. The parameters are stored in this order: 
	\[\mathbf{p} = \left[p_{0}, p_{1}, p_{2}, p_{3}, p_{4}, p_{5}\right], A = \begin{bmatrix}  p_{0} & p_{1} \\ p_{2} & p_{3} \end{bmatrix}, \mathbf{t} = \begin{bmatrix}  p_{4} & p_{5}\end{bmatrix}\]

 \begin{marginfigure}
  \includegraphics[width=\textwidth]{solution2_1.pdf}
  \caption{Solution 2}
 \end{marginfigure}

 \begin{itemize}
  \item Write the function \texttt{affineTransform2D(points, parameters, centerOfRotation)} that transforms \texttt{points} (i.e. the \texttt{fixedImageGrid}).
  \item Create a \texttt{transformedGrid} with the \texttt{affineParameters} provided in the assignment and resample the moving image.
  \item Show the image
  \item When your function \texttt{affineTransform2D(points, parameters, centerOfRotation)} is ready copy it to \texttt{mylibrary.py}
 \end{itemize}
   
 \section{mean squared difference (15 min)}
 \textbf{Goal}: Calculate the similarity between a fixed image and a transformed moving image.
 
 \noindent In registration the similarity between images is optimized. In this workshop we use the mean squared difference between image as similarity metric, which gives a low value for images that are similar. 
  A mean squared difference, i.e. our cost function for registration, is defined: 
  \begin{marginfigure}
  \includegraphics[width=\textwidth]{solution3a_1.pdf}
  \caption{Solution 3a}
 \end{marginfigure}  
  \[C(F,M; \mathbf{p}) = \frac{1}{N}\sum_x{\left(F\left(\mathbf{x}\right) - M\left(T\left(\mathbf{x};\mathbf{p}\right)\right)\right)^2},\]
  with $F$ and $M$ the fixed and moving images, and $N$ the number of points.
 \subsection{translation transform}


 \begin{itemize}
 \item use the \texttt{translationTransform2D} and resample the moving image with a translation of $[-10,-25]$ into the fixed domain. Calculate the mean squared difference between the resampled image and the fixed image. The result should be 5044.2; \textbf{Hint} If you have \texttt{NaN} check the resampled imageData and think how to address Not-A-Numbers.
 \item implement the function \texttt{meanSquaredDifferenceTranslation(fixedImage, movingImage, offset)}
 \item run the script and plot the costfunction as a landscape
 \item copy \texttt{meanSquaredDifferenceTranslation(fixedImage, movingImage, offset)} to \texttt{mylibrary.py}
\end{itemize}

\subsection{affine tranform }
\begin{itemize} 
 \item implement \texttt{meanSquaredDifference}, i.e. for the affine transform
 \item create plots of each parameter sweeping over a range while keeping other parameters at the identity transform
 \item copy \texttt{meanSquaredDifference} to \texttt{mylibrary.py}
 \end{itemize}

  \begin{marginfigure}
  \includegraphics[width=\textwidth]{solution3b_1.pdf}
  \caption{Solution 3b}
 \end{marginfigure}

\section{gradient descent optimization (15 min)}
\textbf{Goal}: Find the transform parameters automatically for optimal similarity between a fixed image and a transformed moving image.\\
 
 \begin{marginfigure}
  \begin{overpic}[width=\textwidth]{costfunctiongradients.pdf}
 \put (24,50) {$\mathbf{p}^k$}
 \put (28,56) {\color[rgb]{1,0,0} $-\frac{\partial C}{\partial p_0}$}
 \put (17,38) {\color[rgb]{1,0,0}$-\frac{\partial C}{\partial p_1}$}
 \put (33,25) {\color[rgb]{0,0,1}$-\frac{\partial C}{\partial \mathbf{p}}$}
 \put (50,0) {$p_0$}
 \put (0,40) {$p_1$}
\end{overpic}
  \caption{Gradient descent}
  \label{fig:direction}
\end{marginfigure}
\begin{marginfigure}
  \begin{overpic}[width=\textwidth]{costfunctionfinitedifferences.pdf}
 \put (24,60) {$p_0^k$}
 \put (24,48) {\color[rgb]{1,0,0}$\Delta_{p_0}$}
 \put (31,56) {\color[rgb]{1,0,0} $\Delta_C$}
 \put (56,58) {\color[rgb]{1,0,0}$\frac{\partial C}{\partial p_0} \approx \frac{\Delta_C}{\Delta_{p_0}}$}
 \put (57,11) {$p_1^k$}
 \put (47,18) {\color[rgb]{1,0,0} $\Delta_C$}
 \put (57,26) {\color[rgb]{1,0,0}$\Delta_{p_1}$}
 \put (15,27) {\color[rgb]{1,0,0}$\frac{\partial C}{\partial p_1} \approx \frac{\Delta_C}{\Delta_{p_1}}$}
 %\put (50,0) {$p_1$}
 %\put (50,38) {$p_0$}
\end{overpic}
  \caption{Gradient by finite differences, Solution 4a}
  \label{fig:finitedifference}
 \end{marginfigure}

 %\input{costfunctiongradients.pdf_tex}
 Registration is formulated as an optimization of the cost function:
 \[\argmin_\mathbf{p} C(\mathbf{p})\]
 Gradient descent is an iterative optimization technique that traverses the cost function landcape by following the steepest gradient direction. The process is formulated:
 \[ \mathbf{p}^{k+1} = \mathbf{p}^{k} + a^k \cdot \mathbf{d}^{k},\]
 with $k$ the iteration number, $a^k$: stepsize, and  $\mathbf{d}^k$: search direction.
 The search direction equals the negative partial derivative of the cost function with respect to the parameters: $\mathbf{d}^k = -\frac{\partial C}{\partial \mathbf{p}}(\mathbf{p}^k)$. This is illustrated by figures \ref{fig:direction} and \ref{fig:finitedifference}.
 \subsection{translation transform}
% \begin{marginfigure}
%  \includegraphics[width=0.45\textwidth]{solution4a_1.pdf}
%  \includegraphics[width=0.45\textwidth]{solution4a_2.pdf}
%  \caption{Solution 4a}
% \end{marginfigure}
% \begin{marginfigure}
%  \includegraphics[width=\textwidth]{solution4a_1.pdf}
%  \caption{Solution 4a}
% \end{marginfigure}
 \begin{marginfigure}
  \includegraphics[width=\textwidth]{solution4a_2.pdf}
  \caption{Solution 4a}
 \end{marginfigure}
 \begin{itemize}
 \item the function \texttt{meanSquaredDifferenceTranslationFiniteDifferenceGradient} calculates the gradient of the cost function at position $\mathbf{p}^k$ by using a finite difference (\texttt{deltas}) on the parameters.
 \item run the script that plots the cost and gradient of the each parameter by sweeping over a range while keeping other parameters at the identity transform
 \item copy \texttt{meanSquaredDifferenceTranslationFiniteDifferenceGradient} to \texttt{mylibrary.py}
  \end{itemize}
\textbf{Question}: verify visually whether the plotted gradients are consistent with the cost function plot.
 
 \subsection{affine transform}
%  \begin{marginfigure}
%  \includegraphics[width=\textwidth]{solution4b_1.pdf}
%  \caption{Solution 4b}
% \end{marginfigure}
 
% \begin{marginfigure} 
%  \includegraphics[width=\textwidth]{solution4b_2.pdf}
%  \caption{Solution 4b}
% \end{marginfigure}

 \begin{itemize} 
 \item this assignment is optional, but please do copy \texttt{meanSquaredDifferenceFiniteDifferenceGradient} to \texttt{mylibrary.py}
 \item run the script that plots the cost and gradient of the each parameter by sweeping over a range while keeping other parameters at the identity transform
  \end{itemize}
\textbf{Question}: why do the sweeps over the first 4 paramters give less smooth cost functions than the sweeps over the last 2 parameters?

  
  \subsection{optimize finite differences}
  \begin{marginfigure}
  \includegraphics[width=\textwidth]{solution4c_1.pdf}
  \caption{Solution 4c}
 \end{marginfigure}

 \begin{itemize} 
 \item implement \texttt{optimizeTranslationFiniteDifference} that uses\\ \texttt{meanSquaredDifferenceTranslationFiniteDifferenceGradient}.
 \item run the optimization for 10 iterations
 \item plot the optimization trajectory in the cost function landscape figure of assignment 3a (it loads the landscape data that was genereated when running assingment 3a, so run it at least once)
 \end{itemize}

\section{optimization by analytic gradients (45 min)}
\textbf{Goal}: improve the computational efficiency of optimization by using an analytically derived gradient of the cost function.\\

Instead of a finite difference we can derive the gradients of the cost function analytically. This involves using the chain rule.  
\[ \frac{\partial C}{\partial \mathbf{p}} = \left.\frac{\partial C}{\partial I}\right|_{I=M(T(\mathbf{x};\mathbf{p}^k))} \cdot \left.\frac{\partial M}{\partial \mathbf{y}}\right|_{\mathbf{y}=T(\mathbf{x};\mathbf{p}^k)} \cdot \left.\frac{\partial T}{\partial \mathbf{p}}\right|_{\mathbf{p}=\mathbf{p}^k }
 ,\]
 with:
   \begin{marginfigure}
  \includegraphics[width=\textwidth]{solution5a_1.pdf}
  \caption{Solution 5a}
 \end{marginfigure}
 \begin{marginfigure} 
  \includegraphics[width=\textwidth]{solution5a_2.pdf}
  \caption{Solution 4a, 5a}
 \end{marginfigure}
  \begin{marginfigure} 
  \includegraphics[width=\textwidth]{solution5a_3.pdf}
  \caption{Solution 4a, 5a}
 \end{marginfigure}
 \begin{itemize}
 \item $\left.\frac{\partial C}{\partial I}\right|_{I=M(T(\mathbf{x};\mathbf{p}^k))}$ the derivative of the cost function with respect to the moving image, evaluated at the transformed moving image.
 \item $\left.\frac{\partial M}{\partial \mathbf{y}}\right|_{\mathbf{y}=T(\mathbf{x};\mathbf{p}^k)}$ the derivative of the moving image with respect to position $\mathbf{y}$, evaluated at $\mathbf{y}$ being a transformed point.
 \item $\left.\frac{\partial T}{\partial \mathbf{p}}\right|_{\mathbf{p}=\mathbf{p}^k }$ the derivative of the transformation with respect to the parameters, evaluated at the current parameter setting $\mathbf{p}^k$.
 \end{itemize}
 
Writing out the chain rule for mean squared differences results in:
\[
 %\frac{\partial C}{\partial \mathbf{p}} &= - \frac{2}{N}\sum_x{\left(F\left(\mathbf{x}\right) - M\left(\mathbf{T}\left(\mathbf{x};\mathbf{p}\right)\right)\right)}\frac{\partial M}{\partial \mathbf{p}}\\
 \frac{\partial C}{\partial \mathbf{p}} = - \frac{2}{N}\sum_x{\left(F\left(\mathbf{x}\right) - M\left(\mathbf{T}\left(\mathbf{x};\mathbf{p}\right)\right)\right)}\left[\frac{\partial M}{\partial \mathbf{y}}\right]\left[\frac{\partial T}{\partial \mathbf{p}}\right],\\
\]
with $\left[\frac{\partial M}{\partial \mathbf{y}}\right]$ the image gradient (row) vector and $\left[\frac{\partial T}{\partial \mathbf{p}}\right]$ the transformation Jacobian matrix.
\subsection{analytic gradient mean squared difference}

\begin{itemize}
 \item transform and show an image like you did in assignment 2, except use \\ \texttt{rw.imageInterpolatorWithGradients} for interpolation. The calculated image gradients are the $\left[\frac{\partial M}{\partial \mathbf{y}}\right]$.
 \item implement \texttt{affineJacobian2DAt}: the partial derivative of the affine transforms $\left[\frac{\partial T}{\partial \mathbf{p}}\right]$
 \item implement the function \texttt{meanSquaredDifferenceWithGradient} for the affine transform, using the chain rule as explained.
 \item create cost and derivative plots of each parameter sweeping over a range while keeping other parameters at the identity transform
 \item veryify that the analytic gradients are similar to the gradients calculated by finite differences
 \end{itemize}
\subsection{optimization of affine parameters}
  \begin{marginfigure}
  \includegraphics[width=\textwidth]{solution5b_1.pdf}
  \caption{Solution 5b}
 \end{marginfigure}
  \begin{marginfigure}
     \includegraphics[width=\textwidth]{solution5b_2.pdf}
  \caption{Solution 5b}
 \end{marginfigure}
  \begin{marginfigure}
  \includegraphics[width=\textwidth]{solution5b_3.pdf}
  \caption{Solution 5b}
 \end{marginfigure}

\begin{itemize}
 \item implement gradient descent using the \texttt{meanSquaredDifferenceWithGradient}
 \item run the optimization and tune the settings \texttt{parameterscales} and \texttt{stepsize} for a good convergence
 \item plot the cost function against the iteration number
 \item show the difference image of the the fixed image and the resampled moving image.
\end{itemize}

\section*{Bonus assignments}
\begin{itemize}
 \item implement a multi-resolution registration algorithm (look at \texttt{rw.blurimage(image)})
 \item implement random sampling, i.e. use less samples each iteration to speed up the calculation of the metric. (type \texttt{np.random} in the spyder's object explorer) 
 \item implement a cost function that calculates the normalized cross correlation 
 \item implement a 3-dimensional registration algorithm
\end{itemize}

\end{document}
