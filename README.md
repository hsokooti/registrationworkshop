# README #

Image registration implementation workshop. Implement a basic registration algorithm in python. Workshop held at the NFBIA summerschool september 2015.

### What is this repository for? ###

* Assignments, documentation, library and scripts for the image registration implementation workshop
* Version 1.0

### How do I get set up? ###

####Requirements:####
* spyder: https://pythonhosted.org/spyder/
* simpleITK (for its reader): http://www.simpleitk.org/SimpleITK/resources/software.html

####Documentation####
* Assignment description: ImageRegistrationWorkshopAssignments.pdf 
* Reference tailored to this workshop: documents/miniReference.html
* presentation at NFBIA (based on VirtualMachine): implementation.odp

####Starting with the workshop:####
* start spyder
* open assignments/pythoninto.py
* open assignments/workshopinto.py
* open assignments/assignment1.py
* open assignments/assignment[...].py

### Who do I talk to? ###

* Floris Berendsen
* Marius Staring
* Stefan Klein
* Josien Pluim