# -*- coding: utf-8 -*-
"""
/*=========================================================================
 *
 *  Copyright elastixteam and contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0.txt
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *=========================================================================*/
 
NFBIA summer school registration workshop

@author: Floris Berendsen
"""

# load the library made for this workshop
import registrationworkshop as rw
# load python's matrix library
import numpy as np 

# read image from disk that serves as our 'fixed' image
fixedImage = rw.readImage('image_A.mhd')

# read image from disk that serves as our 'moving' image
movingImage = rw.readImage('image_B.mhd')


def translationTransform2D(points,offset):
# to be implemented
    return transformedPoints
fixedImageGrid = rw.imageGridToPoints(fixedImage)
offset = np.array([4,-10])
transformedGrid = translationTransform2D(fixedImageGrid,offset)

resultImageData =  rw.imageInterpolator(movingImage, transformedGrid)
resultImage = rw.createImage(**fixedImage)
resultImage['data'] = resultImageData

rw.show_physical_image2D(resultImage)