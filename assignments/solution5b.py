# -*- coding: utf-8 -*-
"""
/*=========================================================================
 *
 *  Copyright elastixteam and contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0.txt
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *=========================================================================*/
 
NFBIA summer school registration workshop

@author: Floris Berendsen
"""

# load the library made for this workshop
import registrationworkshop as rw
# load python's matrix library
import numpy as np 
# load python's plotting library 
import matplotlib.pyplot as plt

from mylibrary import affineTransform2D

from mylibrary import meanSquaredDifferenceWithGradient


# read image from disk that serves as our 'fixed' image
fixedImage = rw.readImage('image_A.mhd')

# read image from disk that serves as our 'moving' image
movingImage = rw.readImage('image_B.mhd')

centerOfRotation = rw.getCenterOfImage(fixedImage)

def optimize(fixedImage, movingImage, initialParameters, parameterScales, centerOfRotation,  stepsize = 5e-3, numberOfIterations=10):
    
    parameters = initialParameters
    #store parameters 0 to 5, cost
    optimizerTrajectory=np.empty((numberOfIterations,parameters.size+1),dtype=float)   
    for iterationNumber in range(numberOfIterations):
        
        metricValue, parameterGradients = meanSquaredDifferenceWithGradient(fixedImage, movingImage, parameters,centerOfRotation)            
        
        optimizerTrajectory[iterationNumber, :-1]= parameters
        optimizerTrajectory[iterationNumber, -1] = metricValue
        
        parameters-=stepsize*parameterScales*parameterGradients
        
        print('{0:d}: m={1:g}'.format(iterationNumber,metricValue))
    return optimizerTrajectory

paramorigin = np.array([1,0,0,1,0,0],dtype='float')

# tune your own parameterScales and stepsize
parameterScales = np.array([0.001,0.001,0.001,0.001,1,1])
stepsize =  1e-2

trajectory = optimize(fixedImage, movingImage, paramorigin, parameterScales, centerOfRotation, stepsize = stepsize, numberOfIterations=50)

print('the resulting parameters are:')
print(trajectory[-1,:])

optFig=plt.figure()
optAx = optFig.add_subplot(1,1,1)
optAx.plot(trajectory[:,-1])

registeredImage = rw.createImage(**fixedImage)
fixedImageGrid = rw.imageGridToPoints(fixedImage)

registeredImage['data'] = rw.imageInterpolator(movingImage, affineTransform2D(fixedImageGrid,trajectory[-1,:-1],centerOfRotation))
rw.show_physical_image2D(registeredImage)

differenceInFixedImage = rw.createImage(**fixedImage)
differenceInFixedImage['data'] = fixedImage['data'] - registeredImage['data']
rw.show_physical_image2D(differenceInFixedImage)

