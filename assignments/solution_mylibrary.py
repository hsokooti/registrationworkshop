# -*- coding: utf-8 -*-
"""
/*=========================================================================
 *
 *  Copyright elastixteam and contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0.txt
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *=========================================================================*/
 
NFBIA summer school registration workshop

@author: you
"""
# load the library made for this workshop
import registrationworkshop as rw
# load python's matrix library
import numpy as np 

# you may remove the line below if it is getting irrating :).
#print("by importing this module all code is executed that is not in a function")


def dummy():
    print("the dummy is executed")

# copy-paste here your functions that are finished and need to be reused

#exercise 1
def translationTransform2D(points,offset):
    return points + offset

#exercise 2
def affineTransform2D(points,parameters,centerOfRotation):
    mat=parameters[:4].reshape([2,2])
    vec=parameters[4:]
    return (points-centerOfRotation).dot(mat) + vec + centerOfRotation

#exercise 3
def meanSquaredDifferenceTranslation(fixedImage, movingImage, offset):
    samplingGridAtFixed = rw.imageGridToPoints(fixedImage)
    transformedSamplingGrid=translationTransform2D(samplingGridAtFixed,offset)
    interpolatorValues, isValidPoint = rw.imageInterpolator(movingImage,transformedSamplingGrid,sparse = True)
    return np.mean((fixedImage ['data'][isValidPoint]-interpolatorValues)**2)

def meanSquaredDifference(fixedI, movingI, parameters,centerOfRotation):
    samplingGridAtFixed = rw.imageGridToPoints(fixedI)
    transformedSamplingGrid=affineTransform2D(samplingGridAtFixed,parameters,centerOfRotation)
    interpolatorValues, isValidPoint = rw.imageInterpolator(movingI,transformedSamplingGrid,sparse=True)
    return np.mean((fixedI['data'][isValidPoint]-interpolatorValues)**2)
    
#exercise 4
def meanSquaredDifferenceTranslationFiniteDifferenceGradient(fixedImage, movingImage, offset, deltas):
    samplingGridAtFixed = rw.imageGridToPoints(fixedImage)
    transformedSamplingGrid=translationTransform2D(samplingGridAtFixed,offset)
    interpolatorValues, isValidPoint = rw.imageInterpolator(movingImage,transformedSamplingGrid,sparse=True)
    metricValue=np.mean((fixedImage['data'][isValidPoint]-interpolatorValues)**2)
    offsetGradients = np.zeros_like(offset)
    for pIndex in range(offset.size):
        currentOffset = offset.copy()
        currentOffset[pIndex] += deltas[pIndex]
        transformedSamplingGrid=translationTransform2D(samplingGridAtFixed,currentOffset)
        interpolatorValues, isValidPoint = rw.imageInterpolator(movingImage,transformedSamplingGrid,sparse=True)
        offsetGradients[pIndex] = np.mean((fixedImage['data'][isValidPoint]-interpolatorValues)**2)
    offsetGradients-= metricValue
    offsetGradients/=deltas
    return metricValue, offsetGradients     

def meanSquaredDifferenceFiniteDifferenceGradient(fixedImage, movingImage, parameters,centerOfRotation,deltas):
    samplingGridAtFixed = rw.imageGridToPoints(fixedImage)
    transformedSamplingGrid=affineTransform2D(samplingGridAtFixed,parameters,centerOfRotation)
    interpolatorValues, isValidPoint = rw.imageInterpolator(movingImage,transformedSamplingGrid,sparse=True)
    metricValue=np.mean((fixedImage['data'][isValidPoint]-interpolatorValues)**2)
    parameterGradients = np.zeros_like(parameters)
    for pIndex in range(parameters.size):
        currentParameters = parameters.copy()
        currentParameters[pIndex] += deltas[pIndex]
        transformedSamplingGrid=affineTransform2D(samplingGridAtFixed,currentParameters,centerOfRotation)
        interpolatorValues, isValidPoint = rw.imageInterpolator(movingImage,transformedSamplingGrid,sparse=True)
        parameterGradients[pIndex] = np.mean((fixedImage['data'][isValidPoint]-interpolatorValues)**2)
    parameterGradients-= metricValue
    parameterGradients/=deltas
    return metricValue, parameterGradients

def optimizeTranslationFiniteDifference(fixedImage, movingImage, initialParameters, numberOfIterations):
    parameters = initialParameters
    #store x, y, cost
    optimizerTrajectory=np.empty((numberOfIterations,parameters.size+1),dtype=float)   
    deltas = np.array([0.001,0.001])
    stepsize = 5e-3 
    for iterationNumber in range(numberOfIterations):
        metricValue =  meanSquaredDifferenceTranslation(fixedImage, movingImage, parameters)
        
        parameterGradients = meanSquaredDifferenceTranslationFiniteDifferenceGradient(fixedImage, movingImage, parameters ,deltas)
        metricValue = meanSquaredDifferenceTranslation(fixedImage, movingImage, parameters)
        
        optimizerTrajectory[iterationNumber, :-1]= parameters
        optimizerTrajectory[iterationNumber, -1] = metricValue
        
        parameters-=stepsize*parameterGradients
        
        print('{0:d}: m={1:g}, g={2}, p={3}'.format(iterationNumber,metricValue,parameterGradients, parameters))
        
    return optimizerTrajectory

#exercise 5
def affineJacobian2DAt(point,centerOfRotation):
    p=point-centerOfRotation
    return np.array([[p[0], 0   , p[1], 0   , 1 , 0],
                     [0   , p[0], 0   , p[1], 0 , 1]],dtype=float)  
                     
def meanSquaredDifferenceWithGradient(fixedImage, movingImage, parameters,centerOfRotation):
    #dS/dmu = dS/dI * dI/dT * dT/dmu
    samplingGridAtFixed = rw.imageGridToPoints(fixedImage)
    transformedSamplingGrid = affineTransform2D(samplingGridAtFixed,parameters,centerOfRotation)
    intensityAndGradients, isValidPoint = rw.imageInterpolatorWithGradients(movingImage,transformedSamplingGrid, sparse=True)
    parameterGradients=np.zeros_like(parameters)
    fixedIntensities = fixedImage['data'][isValidPoint]

    numberOfValidpoints= intensityAndGradients.shape[0]
    for validPointIndex in range(numberOfValidpoints):
        fixedInt= fixedIntensities[validPointIndex]
        movingInt = intensityAndGradients[validPointIndex,0]
        movingGrad = intensityAndGradients[validPointIndex,1:]
        point=transformedSamplingGrid[isValidPoint,:][validPointIndex,:]
        parameterGradients+= -2.0*(fixedInt - movingInt) * movingGrad.dot( affineJacobian2DAt(point,centerOfRotation) )

    parameterGradients/=np.sum(isValidPoint)
    metricValue = np.mean((fixedIntensities-intensityAndGradients[...,0])**2)
    return metricValue, parameterGradients
    
def optimize(fixedI, movingI, initialParameters, parameterScales, centerOfRotation,  stepsize = 5e-3, numberOfIterations=10):
    
    parameters = initialParameters
    #store x, y, cost
    optimizerTrajectory=np.empty((numberOfIterations,parameters.size+1),dtype=float)   
    for iterationNumber in range(numberOfIterations):
        
        metricValue, parameterGradients = meanSquaredDifferenceWithGradient(fixedI, movingI, parameters,centerOfRotation)            
        
        optimizerTrajectory[iterationNumber, :-1]= parameters
        optimizerTrajectory[iterationNumber, -1] = metricValue
        
        parameters-=stepsize*parameterScales*parameterGradients
        
        print('{0:d}: m={1:g}'.format(iterationNumber,metricValue))
    return optimizerTrajectory

