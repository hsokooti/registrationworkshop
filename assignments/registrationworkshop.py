# -*- coding: utf-8 -*-
"""
/*=========================================================================
 *
 *  Copyright elastixteam and contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0.txt
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *=========================================================================*/
 
NFBIA summer school registration workshop

@author: Floris Berendsen
"""
import matplotlib.pyplot as plt
import numpy as np
from scipy.ndimage import gaussian_filter
import SimpleITK as sitk

def readImage(filename):
    """ 
    Read an image in our workshop format. It can read intensity (scalar) and 
    deformation field (vector) images. The reader uses ITK and therefore 
    supports various file formats: http://www.itk.org/Wiki/ITK/File_Formats
    
    Parameters
    ----------
    filename : str
        name of the file to be read
        
    Returns
    -------
    image : dict
        image in our workshop format
    """
    itkImg = sitk.ReadImage(filename)
    origin = itkImg.GetOrigin()[::-1]
    spacing = itkImg.GetSpacing()[::-1]
    extent = itkImg.GetSize()[::-1]    
    data = sitk.GetArrayFromImage(itkImg)

    return createImage(origin,spacing,extent,data)    

def createImage(origin, spacing, extent, data):
    """
    Returns an image defined with physical position and dimensions in our
    workshop format
    
    Returns a dictionary that bundles Image intensities with the spatial
    properties origin, spacing and extent. For convenience this function checks
    if the dimensionality of all the argmuments are consistent.

    Parameters
    ----------
    origin : array_like
        Position of the first voxel in the world coordinates. A voxel position
        is refered to as its center.
    spacing : array_like
        size of the voxels in physical units, e.g. mm
    extent : array_like
        the number of voxels in each direction
    data : array_like 
        The data at the voxels. For an intensity image data.shape must be equal
        to extent. For a vector image data.shape = ( xdims, ydims, ..., vdims ) 
        whereas extent = [ xdims, ydims, ... ], with vdims the dimensionality of 
        the vector.
             
    Returns
    -------
    image : dictionary
        All properties are stored as nd-arrays. The indiviual elements of the 
        arrays cannot be written to. To change properties a new array must be 
        assigned.
        
    Notes
    -----
    We use a dictionary instead of a dedicated class for its ease of use with 
    Spyder' variable explorer.
    An Image dictionary can be manually created by:
    
    >>> image = {'origin' : origin, 'spacing' : spacing, 'extent' : extent, 
                 'data' : data}
    
    A (shallow) copy of an image can be made:
    
    >>> secondImage = createImage(**firstImage)

    Examples
    --------
    
    >>> createImage(origin=np.array([0.0,0.0]),
                    spacing=np.array([1.0,1.0]),
                    extent=np.array([1.0,1.0]),
                    data=np.array([[0.0]]))
    >>> createImage([0.0,0.0],[1.0,1.0],[1.0,1.0],[0.0])
    >>> createImage([0.0,0.0],[1.0,1.0],data = np.array[[0.0]], 
                    extent=(1,1))
    
    """
    
    # Convert lists, tuples and integer arrays to float arrays
    # Try to keep the original references to the data, i.e. without copying
    origin=np.asarray(origin,dtype=float)    
    spacing=np.asarray(spacing,dtype=float)
    extent=np.asarray(extent,dtype=int)
    data=np.asarray(data,dtype=float)
    # After creating an image the individual elements of the array defining the
    # world coordinates cannot be changed anymore. For changes you need to 
    # reassign a new array 
    origin.setflags(write=False)    
    spacing.setflags(write=False)    
    extent.setflags(write=False)    
    data.setflags(write=False)    

    if (not origin.shape == spacing.shape) or (not origin.shape == extent.shape):
        raise ValueError("The shapes of origin, spacing and extent must be equal. Found {0:s} {1:s} {2:s}".format(repr(origin.shape), repr(spacing.shape), repr(extent.shape)))
    if (len(data.shape)>(extent.size+1)) or (not data.shape[:extent.size] == tuple(extent)):
        raise ValueError("The dimensionality of the data is incompatible with the extent. Found {0:s} {1:s}".format(repr(data.shape), repr(tuple(extent))))
    return {'origin' : origin, 'spacing' : spacing, 'extent' : extent, 'data' : data}
    

def getNeighborhood(imageData,index):    
    """
    Helper function for image data interpolators
    
    Returns the n-dimensional patch of 2x2x... image data with its lowercorner at index
    Patches lying (partly) outside the image extent are filled with nearest pixels (zero flux Neumann boundary condition)
    Function is to be used with the linear interpolator. 
    
    Parameters
    ----------
    imageData : array_like
        An N-dimensional array with image intensities
    index : array-like
        A vector with N elements
        
    Returns
    -------
    neighborhood : array_like of size 2x2x...
    """
    ndneighborhoodindex = [np.array([c,c+1],dtype=int,ndmin=direction+1).T for direction,c in enumerate(index[::-1])][::-1]
    ndneighborhoodindex = [np.clip(indices,0,imageData.shape[direction]-1) for direction, indices in enumerate(ndneighborhoodindex)]
    return imageData[ndneighborhoodindex]

def getNeighborhoodView(imageData,index):
    """
    Similar to getNeighborhood(), but returns a 'view' to the data, i.e. no 
    copy is made and writing to the neigborhood array changes the underlying 
    image data.
    No boundary conditions apply and an Error is thrown if the neighborhood 
    reaches over the boundary
    """
    ndneighborhoodindex=[np.s_[lowerIndex:lowerIndex+2] for lowerIndex in index]
    return imageData[ndneighborhoodindex]
    
def intensityAt(continuousIndex,imageData):
    """ 
    Linear Interpolation of the image data at a (real valued) continuousIndex in image data space 
    Parameters
    ----------
    continuousIndex : array_like
        position in (real valued) voxel coordinates. 
    imageData : array_like
        intensity values of the image, e.g. image['data']
    extent : array_like

    Returns
    -------
    intensity : scalar_like
        interpolated value
    """
    index = np.floor(continuousIndex)
    remainderIndex = continuousIndex-index
    neighborhood = getNeighborhood(imageData,index)
    for rem in remainderIndex[::-1]:
        interpIntensityKernel=np.array([1.-rem,rem])
        neighborhood = neighborhood.dot(interpIntensityKernel)
    return neighborhood

def derivativeAt(continuousIndex,imageData,direction):
    """ 
    Calculate the derivative value of the image data at a continuousIndex given 
    a linear interpolating kernel. 
    Parameters
    ----------
    continuousIndex : array_like
        position in (real valued) voxel coordinates. 
    imageData : array_like
        intensity values of the image, e.g. image['data']
    direction : scalar
        

    Returns
    -------
    imageDataDerivative : scalar_like
        derivative of the image along the "direction" axis. The magnitudes of 
        the derivatives are relative to the imageData space
    """
    index = np.floor(continuousIndex)
    remainderIndex = continuousIndex-index
    neighborhood = getNeighborhood(imageData,index)
    directionalKernels = [np.array([-1.,1.]) if d==direction else np.array([1.-rem,rem]) for d,rem in enumerate(remainderIndex)]
    for kernel in directionalKernels[::-1]:
        neighborhood = neighborhood.dot(kernel)       
    return neighborhood
 

def intensityAndGradientAt(continuousIndex,imageData):
    """ 
    Interpolate the image data at a (real valued) continuousIndex in image data space 
    Parameters
    ----------
    continuousIndex : array_like
        position in (real valued) voxel coordinates. 
    imageData : array_like
        intensity values of the image, e.g. image['data']

    Returns
    -------
    imageDataDerivative : scalar_like
        derivative of the image along the "direction" axis. The magnitudes of 
        the derivatives are relative to the imageData space
    """

    index = np.floor(continuousIndex)
    remainderIndex = continuousIndex-index
    #dimensions = continuousIndex['extent']
    value = getNeighborhood(imageData,index)
    #neighborhoodPerDirection=[neighborhood for dummy in range(dimensions)]
    derivatives=[]
    #for derivativeDirection in range(dimensions):
    
    
    # v  dx dy dz : value, derivativeX, derivativeY, derivativeZ
    # -- -- -- -- 
    # iz       dz : using value neighborhood of previous iteration, derivatieve in z is calculated and value is interpolated in z
    # iy    dy iy : using value neighborhood of previous iteration, derivatieve in y is calculated and value and dz are interpolated in y
    # iz dx ix ix : using value neighborhood of previous iteration, derivatieve in x is calculated and value, dy and dz are interpolated in x
    
    # loop over all dimension directions 
    # get the fractional part of each coordinate
    for rem in remainderIndex[::-1]: 
        # the dot operator 'eats' dimensions by starting with the last dimension
        #calculate the interpolating kernel for this dimension direcion
        # a linear interpolating kernel is \/
        interpIntensityKernel=np.array([1-rem,rem])  
        #the derivatives of all previous directions are interpolated by the current direction
        derivatives=[derivative.dot(interpIntensityKernel) for derivative in derivatives]
        #the current directional derivative is obtained by the previous interpolated value 
        # a linear derivative kernel is __--
        derivatives.append(value.dot(np.array([-1,1])))
        value = value.dot(interpIntensityKernel)    
    #return value, np.array(derivatives)[::-1]
    return np.array([value]+derivatives[::-1])
    
def gradientAt(continuousIndex,imageData):
    """ 
    Calculate the derivative value of the image at a continuousIndex given 
    a linear interpolating kernel. 
    Parameters
    ----------
    continuousIndex : array_like
        position in (real valued) voxel coordinates. 
    imageData : array_like
        intensity values of the image, e.g. image['data']

    Returns
    -------
    imageDataDerivative : scalar_like
        derivative of the image along the "direction" axis. The magnitudes of 
        the derivatives are relative to the imageData space
    """
    index = np.floor(continuousIndex)
    remainderIndex = continuousIndex-index
    #dimensions = continuousIndex['extent']
    value = getNeighborhood(imageData,index)
    #neighborhoodPerDirection=[neighborhood for dummy in range(dimensions)]
    derivatives=[]
    #for derivativeDirection in range(dimensions):
    
    # loop over all dimension directions 
    # get the fractional part of each coordinate
    for rem in remainderIndex[:0:-1]: 
        #calculate the interpolating kernel for this dimension direcion
        # a linear interpolating kernel is \/
        interpIntensityKernel=np.array([1-rem,rem])  
        #the derivatives of all previous directions are interpolated by the current direction
        derivatives=[derivative.dot(interpIntensityKernel) for derivative in derivatives]
        #the current directional derivative is obtained by the previous interpolated value 
        # a linear derivative kernel is __--
        derivatives.append(value.dot(np.array([-1,1])))
        value = value.dot(interpIntensityKernel)    

    interpIntensityKernel=np.array([1-remainderIndex[0],remainderIndex[0]])  
    #the derivatives of all previous directions are interpolated by the current direction
    derivatives=[derivative.dot(interpIntensityKernel) for derivative in derivatives]
    #the current directional derivative is obtained by the previous interpolated value 
    # a linear derivative kernel is __--
    derivatives.append(value.dot(np.array([-1,1])))
        
    #return value, np.array(derivatives)[::-1]
    return np.array(derivatives[::-1])

def pointsToContinuousIndices(points,image):
    """
    Convert points in world coordinates to continuous indices of the image data.
    The world coordinates of a voxel refer to the voxel center.
    This function is used in Image interpolators.
    
    Parameters
    ----------
    points : array-like
        n-dimensional array points.shape = (..., pdim) with ... any number of 
        dimensions of arbitrary length and pdim the dimensionality of the 
        image['data']
    image : dictionary
        An image in our workshop format
        
    Returns
    -------
    continuousIndices : array-like
        Takes the same array lay-out as points such that     
        continuousIndices.shape == points.shape
    """
    # point accepts arrays where [x,y,z] are accesed by the last axis, e.g. a meshgrid
    points = np.atleast_2d(points)
    continuousIndices = (points-image['origin'])/image['spacing']
    isValidPoint=np.prod((continuousIndices>=-0.5) * (continuousIndices < 
        image['extent']-0.5), axis=-1,dtype=bool)
    return continuousIndices[isValidPoint,:], isValidPoint    

def imageGridToPoints(image):
    """
    Return a grid of points of the world positions of the voxels of an image. 
    The function requires the origin, spacing and extent of an image. 
    
    Parameters
    ----------
    image : dictionary in our workshop format
        reads keys: 'spacing', 'origin' and 'extent'
    """    

    axesgrid = [np.linspace(origin_elem, 
                            origin_elem + (shape_elem-1) * spacing_elem,
                            shape_elem) 
                for origin_elem, shape_elem, spacing_elem 
                in zip( image['origin'],
                        image['extent'],
                        image['spacing'])]
    #print axesgrid    
    return np.array(np.meshgrid(*axesgrid)).T

def getCenterOfImage(image):
    return image['origin']+image['spacing']*(image['extent']-1)/2.0

def sparseIntensitiesToImageData(intensities,isValidPoint, fillValue=np.nan):
    resultImageData = np.empty(isValidPoint.shape + (intensities.shape[1],),dtype=float)
    resultImageData.fill(fillValue)
    resultImageData[isValidPoint,:] = intensities


def imageInterpolator(image, points, sparse=False, fillvalue=np.nan):
    """
    Linear interpolates image at each point in points.
    
    Parameters
    ----------
    image : dict (in workshop format)
        contains the data at each voxel and the position in physical space
    points : array-like
        n-dimensional array points.shape = (..., pdim) with ... any number of 
        dimensions of arbitrary length and pdim the dimensionality of the 
        image['data']        
    sparse : boolean
        for False: the function returns imageData and if necessary filled with 
        fillValue     
        for True: the function returns sparseImageData and isValidPoint arrays
    fillvalue : scalar (optional)
        intensity values outside the image domain.
        default = NaN

    Returns [default]
    -----------------
    imageData : array_like
        intensity values of the resultImage, e.g. resultImage['data']
        Takes the same array lay-out as points such that     
        imageData.shape = (...)
        
    Returns (if sparse == True)
    ---------------------------
    sparseImageData : array-like
        sampled intensities only for points that lie inside the image domain.
    isValidPoint : bool array 
        Takes the same array lay-out as points such that     
        isValidPoint.shape = (...)

    
    See also
    --------
    imageInterpolatorWithGradients, imageInterpolatorGradients
    
    Examples
    --------
    >>> resultImageData = imageInterpolator(image, gridpoints, fillValue = 0)
    >>> sparseImageData, isValidpoint = 
                        rw.imageInterpolator(img, gridpoints, sparse = True)
    To create an image:
    >>> resultImageData = np.zeros(isValidPoint.shape)
    >>> resultImageData[isValidPoint] = sparseImageData
    >>> validPoints = gridpoints[isValidPoint]
    """
    
    #points=np.atleast_2d(points)
    continuousIndices, isValidPoint = pointsToContinuousIndices(points,image)
    intensities = np.apply_along_axis(intensityAt,-1,continuousIndices,image['data'])
    
    if sparse == True:
        return intensities, isValidPoint
    elif sparse == False:    
        resultImageData = np.empty(isValidPoint.shape,dtype=float)
        resultImageData.fill(fillvalue)
        resultImageData[isValidPoint] = intensities
        return resultImageData

    
def imageInterpolatorWithGradients(image, points, sparse=False, fillvalue = np.nan ):
    """
    Linear interpolates image at each point in points and provides the image 
    gradient vector at that point consistently with linear interpolation.
    
    Parameters
    ----------
    image : dict (in workshop format)
        contains the data at each voxel and the position in physical space
    points : array-like
        n-dimensional array points.shape = (..., pdim) with ... any number of 
        dimensions of arbitrary length and pdim the dimensionality of the 
        image['data']        
    sparse : boolean
        for False: the function returns imageData and if necessary filled with 
        fillValue     
        for True: the function returns sparseImageData and isValidPoint arrays
    fillvalue : scalar (optional)
        intensity values outside the image domain.
        default = NaN

    Returns [default]
    -----------------
    imageData : array_like
        intensity values of the resultImage, e.g. resultImage['data']
        Takes the same array lay-out as points such that     
        imageData.shape = (..., vdim), with vdim the image dimensionality + 1.
        For 2-dimensional input images the output imageData along vdim is 
        [I, dI/dx, dI/dy] at the interpolated point.
        
    Returns (if sparse == True)
    ---------------------------
    sparseImageData : array-like
        sampled intensities only for points that lie inside the image domain.
    isValidPoint : bool array 
        Takes the same array lay-out as points such that     
        isValidPoint.shape = (...)

    
    See also
    --------
    imageInterpolator, imageInterpolatorGradients
    
    Examples
    --------
    >>> resultImageData = imageInterpolator(image, gridpoints, fillValue = 0)
    >>> sparseImageData, isValidpoint = 
                        rw.imageInterpolator(img, gridpoints, sparse = True)
    To create an image:
    >>> resultImageData = np.zeros(isValidPoint.shape)
    >>> resultImageData[isValidPoint] = sparseImageData
    >>> validPoints = gridpoints[isValidPoint]
    """
    continuousIndices, isValidPoint = pointsToContinuousIndices(points,image)
    intensityAndGradients = np.apply_along_axis(intensityAndGradientAt,-1,continuousIndices,image['data'])
    intensityAndGradients[:,1:]/=image['spacing']
    if sparse == True:
        return intensityAndGradients, isValidPoint
    elif sparse == False:    
        resultImageData = np.empty(isValidPoint.shape + (intensityAndGradients.shape[1],),dtype=float)
        resultImageData.fill(fillvalue)
        resultImageData[isValidPoint,:] = intensityAndGradients
        return resultImageData

def imageInterpolatorGradients(image,points, sparse=False, fillvalue = np.nan ):
    """
    Provides at each point in points the image gradient vector consistently 
    with linear interpolation.
    
    Parameters
    ----------
    image : dict (in workshop format)
        contains the data at each voxel and the position in physical space
    points : array-like
        n-dimensional array points.shape = (..., pdim) with ... any number of 
        dimensions of arbitrary length and pdim the dimensionality of the 
        image['data']        
    sparse : boolean
        for False: the function returns imageData and if necessary filled with 
        fillValue     
        for True: the function returns sparseImageData and isValidPoint arrays
    fillvalue : scalar (optional)
        intensity values outside the image domain.
        default = NaN

    Returns [default]
    -----------------
    imageData : array_like
        intensity values of the resultImage, e.g. resultImage['data']
        Takes the same array lay-out as points such that     
        imageData.shape = (..., vdim), with vdim the image dimensionality.
        For 2-dimensional input images the output imageData along vdim is 
        [dI/dx, dI/dy] at the interpolated point.
        
    Returns (if sparse == True)
    ---------------------------
    sparseImageData : array-like
        sampled intensities only for points that lie inside the image domain.
    isValidPoint : bool array 
        Takes the same array lay-out as points such that     
        isValidPoint.shape = (...)

    
    See also
    --------
    imageInterpolator, imageInterpolatorWithGradients
    
    Examples
    --------
    >>> resultImageData = imageInterpolator(image, gridpoints, fillValue = 0)
    >>> sparseImageData, isValidpoint = 
                        rw.imageInterpolator(img, gridpoints, sparse = True)
    To create an image:
    >>> resultImageData = np.zeros(isValidPoint.shape)
    >>> resultImageData[isValidPoint] = sparseImageData
    >>> validPoints = gridpoints[isValidPoint]
    """
    continuousIndices, isValidPoint = pointsToContinuousIndices(points,image)
    intensityGradients = np.apply_along_axis(gradientAt,-1,continuousIndices,image['data'])
    intensityGradients/=image['spacing']
    if sparse == True:
        return intensityGradients, isValidPoint
    elif sparse == False:    
        resultImageData = np.empty(isValidPoint.shape + (intensityGradients.shape[1],),dtype=float)
        resultImageData.fill(fillvalue)
        resultImageData[isValidPoint,:] = intensityGradients
        return resultImageData



def show_physical_image2D(img,alpha=None,ax=None):
    """
    Displays 2-dimensional intensity or vector images on screen. The axes of 
    the figure reflect the physical coordinates of the image.

    Parameters
    ----------
    image : dict (in workshop format)
        contains the data at each voxel and the position in physical space
    ax : (optional) None, matplotlib Axis or list of matplotlib Axis
    
    """
    plotExtent=(img['origin'][1]-0.5*img['spacing'][1],
            img['origin'][1]+(img['extent'][1]-0.5)*img['spacing'][1],           
            img['origin'][0]+(img['extent'][0]-0.5)*img['spacing'][0],
            img['origin'][0]-0.5*img['spacing'][0])
    plotsettings={'extent':plotExtent,'origin':'upper', 'interpolation': 'nearest','alpha': alpha }
    #,'xlabel' : 'world coordinate []', 'ylabel' : 'world coordinate []'}
    if type(ax)==list:
        for vectorindex, currentax in enumerate(ax):
            currentax.imshow(img['data'][...,vectorindex],**plotsettings)
            
    elif ax == None:
        fig=plt.figure()        
        ax=[]
        img3d= np.atleast_3d(img['data'])        
        vectorlen=img3d.shape[-1]  
        for vectorindex in range(vectorlen):
            currentax=fig.add_subplot(1,vectorlen,vectorindex+1)
            currentax.imshow(img3d[...,vectorindex],**plotsettings)
            ax.append(currentax)
            #fig.colorbar(ax)
    else:
        ax.imshow(img['data'],**plotsettings)
        
    #fig.colorbar(plt.gca())
    return ax


    
def blurimage(img,sigma):
    blurred = createImage(data=gaussian_filter(img['data'],sigma), origin = img['origin'], spacing = img['spacing'], extent = img['extent'])
    return blurred
