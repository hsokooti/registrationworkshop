# -*- coding: utf-8 -*-
"""
@author: you
"""
# load the library made for this workshop
import registrationworkshop as rw
# load python's matrix library
import numpy as np 

# you may remove the line below if it is getting irrating :).
print("by importing this module all code is executed that is not in a function")


def dummy():
    print("the dummy is executed")

# copy-paste here your functions that are finished and need to be reused