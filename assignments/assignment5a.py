# -*- coding: utf-8 -*-
"""
/*=========================================================================
 *
 *  Copyright elastixteam and contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0.txt
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *=========================================================================*/
 
NFBIA summer school registration workshop

@author: Floris Berendsen
"""

# load the library made for this workshop
import registrationworkshop as rw
# load python's matrix library
import numpy as np 
# load python's plotting library 
import matplotlib.pyplot as plt

# load our previously defined translationTransform2D
from mylibrary import affineTransform2D
# load our previously defined meanSquaredDifferenceFiniteDifferenceGradient
from mylibrary import meanSquaredDifferenceFiniteDifferenceGradient

# read image from disk that serves as our 'fixed' image
fixedImage = rw.readImage('image_A.mhd')

# read image from disk that serves as our 'moving' image
movingImage = rw.readImage('image_B.mhd')



def affineJacobian2DAt(point, centerOfRotation ):
    # to be implemented
    # Hint: make sure that jacobian.shape = (2,6)
    return jacobian
                     
def meanSquaredDifferenceWithGradient(fixedImage, movingImage, parameters, centerOfRotation):
    # to be implemented

    #dC/dp = dC/dM * dI/dx * dT/dp

    return metricValue, parameterGradients
    
centerOfRotation = rw.getCenterOfImage(fixedImage)

testrange=np.linspace(-5,5,10)
# you might want to tune the parameterscales
parameterscales=np.array([1.0,1.0,1.0,1.0,1.0,1.0])

paramorigin = np.array([1,0,0,1,0,0],dtype='float')
costfig = plt.figure()
gradfig = plt.figure()

for parameterindex in range(6):
    currentparam=paramorigin.copy()
    parametersweep=testrange*parameterscales[parameterindex]
    costlandscape=[]
    gradlandscape=[]
    fdgradlandscape=[]
    for sweep in parametersweep:
        currentparam[parameterindex]=paramorigin[parameterindex]+sweep
        print(currentparam)
        metricValue, parameterGradients = meanSquaredDifferenceWithGradient(fixedImage, fixedImage, currentparam,centerOfRotation)
        fdmetricValue, fdparameterGradients = meanSquaredDifferenceFiniteDifferenceGradient(fixedImage, fixedImage, currentparam,centerOfRotation,0.001 * parameterscales)
        costlandscape.append(metricValue)
        gradlandscape.append(parameterGradients[parameterindex])
        fdgradlandscape.append(fdparameterGradients[parameterindex])
    costax = costfig.add_subplot(6,1,parameterindex+1)
    costax.plot(parametersweep, costlandscape, label='cost function')
    gradax = gradfig.add_subplot(6,1,parameterindex+1)
    gradax.plot(parametersweep,gradlandscape,label='analytic gradient')
    gradax.plot(parametersweep,gradlandscape,label='finite difference gradient')    
    #gradax.plot((parametersweep[1:]+parametersweep[:-1])/2.0,np.diff(costlandscape)/np.diff(parametersweep),label='finite difference of cost plot')
    
gradfig.legend(*gradax.get_legend_handles_labels())
costfig.legend(*costax.get_legend_handles_labels())
    
