# -*- coding: utf-8 -*-
"""
/*=========================================================================
 *
 *  Copyright elastixteam and contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0.txt
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *=========================================================================*/
 
NFBIA summer school registration workshop

@author: Floris Berendsen
"""

# load the library made for this workshop
import registrationworkshop as rw
# load python's matrix library
import numpy as np 
# load python's plotting library 
import matplotlib.pyplot as plt
# load specialized funtions for 3d plots 
from mpl_toolkits.mplot3d import Axes3D

# load our previously defined translationTransform2D
from mylibrary import translationTransform2D


# read image from disk that serves as our 'fixed' image
fixedImage = rw.readImage('image_A.mhd')

# read image from disk that serves as our 'moving' image
movingImage = rw.readImage('image_B.mhd')


fixedImageGrid = rw.imageGridToPoints(fixedImage)
transformedGrid = translationTransform2D(fixedImageGrid,[-10,-25])

resultImageData =  rw.imageInterpolator(movingImage, transformedGrid)

# find the formula to calculate the mean squared difference 
isValidGridPoint= ~np.isnan(resultImageData) # boolean array; True for a number
squaredDiffImagedata=(fixedImage['data']-resultImageData)**2
print(np.mean(squaredDiffImagedata[isValidGridPoint]))
# or
print(np.nanmean((fixedImage['data']-resultImageData)**2))
# result should be 5044.18440048

def meanSquaredDifferenceTranslation(fixedImage, movingImage, offset):
    samplingGridAtFixed = rw.imageGridToPoints(fixedImage)
    transformedSamplingGrid=translationTransform2D(samplingGridAtFixed,offset)
    interpolatorValues, isValidPoint = rw.imageInterpolator(movingImage,transformedSamplingGrid,sparse = True)
    return np.mean((fixedImage ['data'][isValidPoint]-interpolatorValues)**2)


# calculate the cost function for all combinations of x- and y-offset between
# -10 and 10 in 20 steps
costlandscape = np.zeros((20,20),dtype=float)
xsweep = np.linspace(-10,10,20)
ysweep = np.linspace(-10,10,20)
for xindex, xoffset in enumerate(xsweep):
    print("progress: {0:d}/{1:d}".format(xindex,len(xsweep)))
    for yindex, yoffset in enumerate(ysweep):
        offset = np.array([xoffset,yoffset])
        costlandscape[xindex,yindex] = meanSquaredDifferenceTranslation(fixedImage, movingImage, offset)

# save the landscape data for later reuse in assignment 4c
with open('translationLandscape.npz','wb') as outputFile:
    np.savez(outputFile, costlandscape=costlandscape, xsweep=xsweep, ysweep=ysweep)


# plot the landscape in a figure
landscapeFig = plt.figure()
landscapeAx = landscapeFig.add_subplot(1,1,1, projection='3d')
landscapeAx.plot_wireframe(xsweep[:,None],ysweep[None,:],costlandscape)
landscapeAx.set_xlabel('x offset')
landscapeAx.set_ylabel('y offset')
landscapeAx.set_zlabel('cost')
