# -*- coding: utf-8 -*-
"""
/*=========================================================================
 *
 *  Copyright elastixteam and contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0.txt
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *=========================================================================*/
 
NFBIA summer school registration workshop

@author: Floris Berendsen
"""

# load the library made for this workshop
import registrationworkshop as rw
# load python's matrix library
import numpy as np 
# load python's plotting library 
import matplotlib.pyplot as plt

# load our previously defined meanSquaredDifferenceTranslation
from mylibrary import meanSquaredDifferenceTranslation
# load our previously defined meanSquaredDifferenceTranslationFiniteDifferenceGradient
from mylibrary import meanSquaredDifferenceTranslationFiniteDifferenceGradient

# read image from disk that serves as our 'fixed' image
fixedImage = rw.readImage('image_A.mhd')

# read image from disk that serves as our 'moving' image
movingImage = rw.readImage('image_B.mhd')

def optimizeTranslationFiniteDifference(fixedImage, movingImage, initialParameters, numberOfIterations):
    parameters = initialParameters
    #store x, y, cost
    optimizerTrajectory=np.empty((numberOfIterations,parameters.size+1),dtype=float)   
    deltas = np.array([0.001,0.001])
    stepsize = 5e-3 
    for iterationNumber in range(numberOfIterations):
        
        metricValue, parameterGradients = meanSquaredDifferenceTranslationFiniteDifferenceGradient(fixedImage, movingImage, parameters, deltas)
        
        optimizerTrajectory[iterationNumber, :-1]= parameters
        optimizerTrajectory[iterationNumber, -1] = metricValue
        
        parameters-=stepsize*parameterGradients
        
        print('{0:d}: m={1:g}, g={2}, p={3}'.format(iterationNumber,metricValue,parameterGradients, parameters))
        
    return optimizerTrajectory


initialOffset = np.array([0.0,0.0])
TrajectoryFD = optimizeTranslationFiniteDifference(fixedImage, movingImage, initialOffset, numberOfIterations=10)

# load the landscape data as was calculated in assignment 3a
# declare the variables to read
xsweep=None
ysweep=None
costlandscape=None
with open('translationLandscape.npz','rb') as inputFile:
    npzfile = np.load(inputFile)
    xsweep = npzfile['xsweep']
    ysweep = npzfile['ysweep']
    costlandscape = npzfile['costlandscape']
    
# plot the landscape as a mesh
landscapeFig = plt.figure()
landscapeAx = landscapeFig.add_subplot(1,1,1, projection='3d')
landscapeAx.plot_wireframe(xsweep[:,None],ysweep[None,:],costlandscape)# xlabel='x offset',ylabel='y offset', zlabel='cost')

# plot the trajectory of the optimizer in the landscape figure
landscapeAx.plot(TrajectoryFD[:,0],TrajectoryFD[:,1],TrajectoryFD[:,2],'-*k')
landscapeAx.set_xlabel('x offset')
landscapeAx.set_ylabel('y offset')
landscapeAx.set_zlabel('cost')