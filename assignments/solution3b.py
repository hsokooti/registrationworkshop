# -*- coding: utf-8 -*-
"""
/*=========================================================================
 *
 *  Copyright elastixteam and contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0.txt
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *=========================================================================*/
 
NFBIA summer school registration workshop

@author: Floris Berendsen
"""

# load the library made for this workshop
import registrationworkshop as rw
# load python's matrix library
import numpy as np 
# load python's plotting library 
import matplotlib.pyplot as plt
# load specialized funtions for 3d plots 
from mpl_toolkits.mplot3d import Axes3D

# load our previously defined affineTransform2D
from mylibrary import affineTransform2D


# read image from disk that serves as our 'fixed' image
fixedImage = rw.readImage('image_A.mhd')

# read image from disk that serves as our 'moving' image
movingImage = rw.readImage('image_B.mhd')

centerOfRotation = rw.getCenterOfImage(fixedImage)

def meanSquaredDifference(fixedImage, movingImage, parameters,centerOfRotation):
    samplingGridAtFixed = rw.imageGridToPoints(fixedImage)
    transformedSamplingGrid=affineTransform2D(samplingGridAtFixed,parameters,centerOfRotation)
    interpolatorValues, isValidPoint = rw.imageInterpolator(movingImage,transformedSamplingGrid,sparse=True)
    return np.mean((fixedImage['data'][isValidPoint]-interpolatorValues)**2)

print(meanSquaredDifference(fixedImage,movingImage,np.array([1,0,0,1,0,0]),centerOfRotation))

testrange=np.linspace(-10,10,20)

# you might want to tune the parameterscales
parameterscales=np.array([1.0,1.0,1.0,1.0,1.0,1.0])
#parameterscales=np.array([0.01,0.01,0.01,0.01,1,1])

paramorigin = np.array([1,0,0,1,0,0],dtype='float')
costfig = plt.figure()
for parameterindex in range(6):
    currentparam=paramorigin.copy()
    parametersweep=testrange*parameterscales[parameterindex]
    costlandscape=[]
    for sweep in parametersweep:
        currentparam[parameterindex]=paramorigin[parameterindex]+sweep
        print(currentparam)
        costlandscape.append(meanSquaredDifference(fixedImage,fixedImage,currentparam,centerOfRotation))    
    ax = costfig.add_subplot(6,1,parameterindex+1)
    ax.plot(parametersweep,costlandscape,label='cost function')
    ax.set_ylabel('p[{0:d}]'.format(parameterindex))
costfig.legend(*ax.get_legend_handles_labels())
