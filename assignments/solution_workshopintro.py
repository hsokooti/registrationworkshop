# -*- coding: utf-8 -*-
"""
/*=========================================================================
 *
 *  Copyright elastixteam and contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0.txt
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *=========================================================================*/
 
NFBIA summer school registration workshop

@author: Floris Berendsen
"""

# load the library made for this workshop
import registrationworkshop as rw
# load python's matrix library
import numpy as np 
# write your own library during this course. Open mylibrary.py and look inside!
from mylibrary import dummy # load the dummy function only
# the loaded function can be executed like this:
dummy()

# write your one code here by uncommenting each command 
# and filling in the "..."

# read image from disk that serves as our 'fixed' image
fixedImage = rw.readImage('image_A.mhd')

# double click fixedImage dictionary in the variable explorer, right click on data -> show image
rw.show_physical_image2D(fixedImage)

# read image from disk that serves as our 'moving' image
movingImage = rw.readImage('image_B.mhd')

# show the image
rw.show_physical_image2D(movingImage)

# create a grid of 2d-points from the voxel positions in the fixed image
fixedImageGrid = rw.imageGridToPoints(fixedImage)

# using this grid, the moving image can be interpolated
movingToFixedImageData =  rw.imageInterpolator(movingImage, fixedImageGrid)

# to combine movingToFixedImageData with a physical position and size we
# create an image with the same properties as the fixed image
movingToFixedImage = rw.createImage(origin  = fixedImage['origin'], 
                                    spacing = fixedImage['spacing'],
                                    extent  = fixedImage['extent'],
                                    data    = movingToFixedImageData)

# but replace the data by our interpolated data
rw.show_physical_image2D(movingToFixedImage)

# Alignment of images can be inspected by looking at the difference image
differenceInFixedImage = rw.createImage(**fixedImage) # ** is to use a dictionary as named arguments
differenceInFixedImage['data'] = fixedImage['data'] - movingToFixedImageData
rw.show_physical_image2D(differenceInFixedImage)



